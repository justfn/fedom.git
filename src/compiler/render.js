/* 渲染方法 
  支持 vary / list / Promise / fdNode / Node / text 值 
*/
import VaryValue from "../featrues/VaryModel/VaryValue.js";
import { 
  isVaryValue,
  isArrayValue,
  isPromiseValue,
  isFunctionValue,
  isFdNode, 
  isNodeValue,
  isTextValue,
  isEmptyValue,
} from "../utils/judge.js";
import { 
  fillNodeChild, 
  fillTextChild, 
  markListStart, 
  // markPromiseNode,
} from "./child/childUtil.js";
import varyChildValue from "../featrues/VaryAction/varyChildren.js";
import {
  scopeField,
  getScopeName,
} from "../featrues/Component/scopeCpnt.js";

/* ** 渲染内容 
arguments: 
  Node,父节点 
  List|VV|Promise|FNode|Node|AnyAsText,子节点
returns: List|VV|FNode|Node 
*/
const msg_todo_render = '未预期的render调用'; 
function fillChildIntoParent(parentNd, LVPFNT, fdWrap=null, vvWrap=null){
  // 包装类型1: List 
  if ( isArrayValue(LVPFNT) ) {
    if (vvWrap) { 
      varyChildValue({
        varyChild: vvWrap, 
        // 数组子节点,标记起始位置,便于后续更新
        patchNodeForList: markListStart(parentNd),
        patchNodeForText: null, 
      });
    }
    
    LVPFNT.forEach(lstItm=>{ 
      fillChildIntoParent(
        parentNd, 
        lstItm, 
        null,
        null,
      ); 
    })
    return vvWrap || fdWrap || LVPFNT;
  }
  
  // 包装类型2: VV 
  if ( isVaryValue(LVPFNT) ) {
    if (vvWrap) {
      throw new Error('mutil vv')
    }
    
    return fillChildIntoParent(
      parentNd, 
      LVPFNT.get(true), 
      fdWrap, 
      LVPFNT,
    );
  }
  // 包装类型[vv的便捷方式之一]: Promise 
  if ( isPromiseValue(LVPFNT) ) {
    let v_val = VaryValue(document.createComment("fedom: unfinished promise"));
    let scopeName = getScopeName();
    LVPFNT.then(
      (val)=>{
        if ( isFunctionValue(val) ) {
          val[scopeField] = scopeName;
          let Fn = val; 
          // <val /> 不可用 
          val = <Fn />;
        }
        v_val.$$ = val;
      },
      (val)=>{
        if ( isFunctionValue(val) ) {
          val[scopeField] = scopeName;
          let Fn = val;
          val = <Fn />;
        }
        v_val.$$ = val;
        // return Promise.reject(err);
      }
    )
    return fillChildIntoParent(
      parentNd, 
      v_val, 
      null, 
      null,
    );
  }
  
  // 包装类型3: FNode 
  if ( isFdNode(LVPFNT) ) {
    return fillChildIntoParent(
      parentNd, 
      LVPFNT.realNode, 
      LVPFNT, 
      vvWrap,
    );
  }
  
  // 渲染1: Node节点 
  if ( isNodeValue(LVPFNT) ) {
    fillNodeChild(parentNd, LVPFNT);
    varyChildValue({
      varyChild: vvWrap, 
      patchNodeForList: null,
      patchNodeForText: null, 
    });
    return vvWrap || fdWrap || LVPFNT;
  }
  // 渲染2: AnyAsText文本 
  else {
    if ( isEmptyValue(LVPFNT) ) { 
      LVPFNT = '' 
    }
    let patchNodeForText = fillTextChild(
      parentNd, 
      LVPFNT
    ); 
    varyChildValue({
      varyChild: vvWrap, 
      patchNodeForText, 
      patchNodeForList: null,
    });
    return vvWrap || fdWrap || patchNodeForText;
  }
} 
function render(LVPFNT, parentNd){
  // console.log("000 render ", LVPFNT, parentNd )
  return fillChildIntoParent(
    parentNd, 
    LVPFNT, 
  );
} 
export default render;



