/* 事件绑定 
*/
import {
  isCommentNode, 
  isFDComponent, 
} from "../../utils/judge.js";
import componentAttrs from "./attrsCpnt.js";
import { 
  addClassAttr, 
  addStyleAttr, 
  addEventAttr, 
  addOtherAttr, 
  addRefAttr, 
} from "./attrsCommon.js";
import varyAttrIf from "../../featrues/VaryAction/varyAttrFdIf.js";
import attrVaryShow from "../../featrues/VaryAction/varyAttrFdShow.js";

function bindAttrs(fdNode){
  let {
    tagName, 
    realNode, 
    attrs, 
  } = fdNode;
  varyAttrIf(fdNode, attrs);
  attrVaryShow(fdNode, attrs);
  if ( isFDComponent(tagName) ) { 
    componentAttrs(fdNode); 
    return ; 
  }
  if (isCommentNode(realNode)) { return ; }
  
  
  for(let key in attrs){
    const val = attrs[key];
    
    if (val===undefined || val===null) { val = ''; }
    
    /* brance: class */
    if (key==='className') {
      addClassAttr(fdNode, val);
      continue;
    }
    /* brance: style */
    if (key==='style') {
      addStyleAttr(fdNode, val);
      continue;
    }
    /* brance: event */
    if (/^on(\w+)$/.test(key)) {
      addEventAttr(fdNode, RegExp.$1.toLowerCase(), val);
      continue;
    }
    /* brance: ref_callback */
    if (key==='ref' ) {
      addRefAttr(fdNode, val);
      continue; 
    }
    /* brance: children */
    if ( key==='children') {
      continue; 
    }
    
    /* brance: 其他属性 */
    addOtherAttr(fdNode, key, val);
  };
}

export default bindAttrs;
