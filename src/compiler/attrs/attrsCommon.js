/* 处理通用属性属性 
*/

import { 
  isVaryValue,
  isStringValue, 
  isObjectValue, 
  isFunctionValue,
  isArrayValue, 
  isPromising, 
} from "../../utils/judge.js";
import {
  setElemAttr,
} from "../../utils/dom.js";
import {
  varyAttrClassStr,
  varyAttrClassArr,
  varyAttrClassListItm,
} from "../../featrues/VaryAction/varyAttrClass.js";
import {
  varyAttrStyleStr,
  varyAttrStyleObj,
  varyAttrStyleOval,
} from "../../featrues/VaryAction/varyAttrStyle.js";
import {
  varyAttrOtherVal,
} from "../../featrues/VaryAction/varyAttrOther.js";

export function addRefAttr(fdNode, value ){
  if (isPromising(value)) {
    value.resolve(fdNode.realNode);
    return ;
  }
  
  if ( isFunctionValue(value) ) { 
    value(fdNode.realNode);
    return ; 
  }
  
  
} 

export function addClassAttr(fdNode, value, varyAttr=null){
  if (!value) { return ; }
  
  if (isVaryValue(value)) {
    addClassAttr(fdNode, value.get(true), value);
    return ;
  }
  
  // 出口1：列表 
  if ( isArrayValue(value) ) {
    value.forEach((itm,idx)=>{
      addClassAttr(fdNode, itm, varyAttr);
      
      varyAttrClassListItm(fdNode, itm);
    })
    
    /* ** Features: 
    */
    varyAttrClassArr(fdNode, varyAttr)
    
    return '';
  }
  // 出口2：字符串 
  if ( isStringValue(value) ) {
    value = value.trim();
    let strList = value.split(/\s+/)
    if ( strList.length>1 ) {
      addClassAttr(fdNode, strList, varyAttr);
      return ;
    }
    
    if (value) { fdNode.realNode.classList.add(value); }
    
    /* ** Features: 
    */
    varyAttrClassStr(fdNode, varyAttr);
    
    return value;
  }
  
  // 其他: 默认转换为字符串处理 
  return addClassAttr(fdNode, value+'', varyAttr);
} 

export function addStyleAttr(fdNode, value, varyAttr){
  if (isVaryValue(value)) {
    addStyleAttr(fdNode, value.get(true), value);
    return ;
  }
  
  // 出口1：
  if ( isStringValue(value) ) {
    fdNode.realNode.setAttribute("style", value);
    
    /* ** Features: 
    */
    varyAttrStyleStr(fdNode, varyAttr);
    
    return ;
  }
  // 出口2：
  if ( isObjectValue(value) ) {
    for(var ky in value){
      let vl = value[ky];
      
      /* ** Features: 
      */
      vl = varyAttrStyleOval(fdNode, ky, vl);
      
      fdNode.realNode.style[ky] = vl;
    };
    
    /* ** Features:  
    */
    varyAttrStyleObj(fdNode, varyAttr);
    
    return ;
  }
  
  console.warn('# todo attrs style', fdNode, value);
} 

export function addEventAttr(fdNode, evtName, listener){
  
  fdNode.realNode.addEventListener(evtName, (evt)=>{
    return listener(evt);
  })
  
} 

const msg_error_todo = 'todo attrs other';
export function addOtherAttr(fdNode, key, val, varyAttr){
  if ( isVaryValue(val) ) {
    varyAttrOtherVal(fdNode, key, val);
    return addOtherAttr(fdNode, key, val.get(true), val);
  }

  let {
    realNode, 
  } = fdNode;
  try {
    setElemAttr(realNode, key, val);
  } 
  catch (err) {
    console.warn( err, msg_error_todo, realNode, key, val);
  } 
} 


