import { 
  isEmptyValue, 
  isObjectValue, 
  isArrayValue, 
} from "../../utils/judge.js";


/* 填充子节点 
*/
export function fillNodeChild(parentNode, node){
  parentNode.appendChild(node);
  return node;
} 

/* 处理文本值 
*/
export function trimTextValue(val){
  if (isEmptyValue(val)) { return ''; }
  
  let rst = '';
  try {
    rst = val.toString();
  } 
  catch (e) {
    rst = val + '';
  } 
  // // 去掉空格 
  // let rst1 = rst.trim();
  // // 只有空格时 不处理 
  // if (!(rst && !rst1)) { rst = rst1; }
  return rst+'';
} 
/* 填充文本节点
*/
export function fillTextChild(parentNode, text){
  if ( isObjectValue(text) || isArrayValue(text) ) {
    try { text = JSON.stringify(text) } 
    catch (e) {
      console.warn('无法渲染的对象');
      text = text+'';
    } 
  }
  else {
    text = text+'';
    text = trimTextValue(text);
  }
  
  let txtNode = document.createTextNode(text);
  parentNode.appendChild(txtNode);
  return txtNode;
} 

/* 标记列表起始位置 
*/
export function markListStart(parentNode){
  let commentNode = document.createComment("fedom: start of array child for position");
  parentNode.appendChild(commentNode);
  return commentNode;
} 

/* 标记未结束的promise 
export function markPromiseNode(parentNode){
  let commentNode = document.createComment("fedom: unfinished promise");
  parentNode.appendChild(commentNode);
  return commentNode;
} 
*/



