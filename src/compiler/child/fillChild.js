/* 填充子节点 
*/
import { 
  isVaryValue, 
  isStringValue, 
  isNumberValue, 
  isArrayValue, 
  isNodeValue, 
  isEmptyValue,  
  isTextValue,  
  isCommentNode,  
  isFDComponent, 
} from "../../utils/judge.js";
import { 
  fillTextChild, 
  markListStart, 
} from "./childUtil.js";
import render from "../render.js";


function fillChildren(fdNode){
  // console.log("000000000 fillChildren", fdNode)
  let {
    realNode, 
    children, 
  } = fdNode; 
  // 注释节点: 不处理子节点 
  if (isCommentNode(realNode)) { return; }
  // 无子节点:  
  if ( fdNode.children.length===0 ) { return; }
  // 组件子节点由用户控制插入 
  if ( isFDComponent(fdNode.tagName) ) { return ; }
  
  // 处理子节点 
  render(children, realNode);
  
  // children.forEach(child=>{ fillChild(fdNode, child, null); })
} 
export default fillChildren;



