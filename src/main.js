/* 入口文件 
*/
import * as judges from "./utils/judge.js";
import * as domUtils from "./utils/dom.js";
import _Promising from "./utils/Promising.js";
import _compiler from "./compiler/compiler.js";
import _render from "./compiler/render.js";
import _onMounted from "./featrues/Lifecycles/onMounted.js";
import _onReused from "./featrues/Lifecycles/onReused.js";
import _onUnmount from "./featrues/Lifecycles/onUnmount.js";
import _Component from "./featrues/Component/Component.js";
import _VaryValue from "./featrues/VaryModel/VaryValue.js";
import _VaryMap from "./featrues/VaryModel/VaryMap.js";
import _VaryList from "./featrues/VaryModel/VaryList.js";
import _Router from "./router/Router.js";
import * as routerUtils from "./router/routerUtils.js";

export const compiler = _compiler;
export const render = _render;
export const onMounted = _onMounted;
export const onUnmount = _onUnmount;
export const onReused = _onReused;
export const Component = _Component;
export const VaryValue = _VaryValue;
export const VaryList = _VaryList;
export const VaryMap = _VaryMap;
export const Router = _Router;
export const DecorateSync = routerUtils.DecorateSync;
export const DecorateAsync = routerUtils.DecorateAsync;
export const DecorateSyncMore = routerUtils.DecorateSyncMore;
export const DecorateAsyncMore = routerUtils.DecorateAsyncMore;
export const Promising = _Promising;
export const utils = {
  ...judges,
  html: (...args)=>domUtils.parseRichText(...args),
};

window.$fd = {
  compiler,
  render,
  onMounted,
  onUnmount,
  onReused,
  Component,
  VaryValue,
  VaryList,
  VaryMap,
  Router,
  DecorateSync,
  DecorateAsync,
  DecorateSyncMore,
  DecorateAsyncMore,
  Promising,
  utils,
};
export default window.$fd;

