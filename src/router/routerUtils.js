import { 
  isNumberValue,
} from "../utils/judge.js";
import Router from "./Router.js";
import { 
  changeMissSwitcher, 
} from "./Router.js";

/* 路由切换时机类型 
*/
export const hct_types = {
  change_start: 'hct_change_start',
  render_start: 'hct_render_start',
  render_end: 'hct_render_end',
  render_error: 'hct_render_error',
  cache_show: 'hct_cache_show',
  cache_same: 'hct_same_cache',
}


/* 格式化传入的路由配置 
*/
export function formatRoutes(routes, ...args){
  let [
    prePath='', 
    routeMap={}, 
    routeList=[], 
    level=0
  ] = args;
  routes.forEach((pathOption,idx)=>{
    let pathKey = pathOption.path;
    if (!pathKey) {
      console.error(pathOption)
      throw new Error('未指定路由地址 path')
    }
    if ( !/^\//.test(pathKey) ) { 
      pathKey = `/${pathKey}`; 
    }
    if ( prePath!=='/' ) {
      pathKey = prePath + pathKey;
    }
    if (pathOption.alias) {
      let _opt = { ...pathOption }
      routeMap[_opt.alias] = _opt;
      pathOption = _opt;
    }
    routeMap[pathKey] = pathOption;
    routeList.push({
      ...pathOption,
      level,
      path: pathKey,
    })
    let children = pathOption.children;
    if (children) {
      formatRoutes(children, pathKey, routeMap, routeList, level+1)
    }
  })
  return {
    routeMap: routeMap,
    routeList: routeList,
  };
}


/* 解析hash部分的内容
*/
export function parseURL(fullUrl){
  fullUrl = fullUrl || window.location.href; 
  
  // https://aa.bb.com/dd/ee/ff?key1=val1#/gg/hh?key2=val2
  let result = {
    pathFull: '',  // '/dd/ee/ff?key1=val1'
    pathName: '',  // '/dd/ee/ff'
    pathQryStr: '', // '?key1=val1'
    pathQuery: {}, // { key1: 'val1' }
    hashFull: '',  // '/gg/hh?key2=val2'
    hashPath: '',  // '/gg/hh'
    hashQryStr: '',  // '?key2=val2'
    hashQryStr: '',  // '?key2=val2'
    hashQuery: {}, // { key2: val2 }
  }
  let arr1 = fullUrl.split('#');
  let _path = arr1[0] || '';
  result.pathFull = _path.split('/').slice(3).join('/') || '/';
  let [ _p, ...rest0 ] = result.pathFull.split('?');
  result.pathName = _p ?? '';
  let _query1 = rest0.join('?');
  if (_query1) {
    result.pathQryStr = '?' + _query1;
  }
  if ( _query1 ) {
    _query1.split('&').forEach((itm,idx)=>{
      let [key, ...rest1] = itm.split('=')
      result.pathQuery[key] = rest1.join('=') ?? '';
    })
  }
  let _hash = arr1.slice(1).join('#');
  if (!_hash) { return result; }
  
  result.hashFull = _hash;
  let [ hPath, ...rest2] = _hash.split('?');
  result.hashPath = hPath;
  if ( rest2.length ) {
    let _query2 = rest2.join('?');
    if ( _query2 ) {
      _query2.split('&').forEach((itm,idx)=>{
        let [key, ...rest3] = itm.split('=')
        result.hashQuery[key] = rest3.join('=') ?? '';
      })
    }
  }
  
  return result;
}
/* 组装hash路由参数 
*/
export function joinHash(hashPath, hashQuery, isFull=true){
  let url = '';
  if (isFull) {
    const { origin, pathname, search, } = window.location;
    let pathName = origin+pathname+search;
    url = pathName 
  }
  url += `#${hashPath}`;
  let qry = '';
  let keyNum = 0; 
  for(let key in hashQuery){
    keyNum++;
    let val = hashQuery[key];
    qry += `&${key}=${val}`
  };
  if (keyNum>0) { 
    qry = '?' + qry.slice(1) 
  }
  return url + qry;
} 


/* 切换路由-push 
*/
export function hashPush(hashPath, hashQuery={}){
  const router = Router();
  router[changeMissSwitcher]();
  let hashUrl = joinHash(hashPath, hashQuery, false);
  location.hash = hashUrl;
  // location.assign(url)
} 
/* 切换路由-replace 
*/
export function hashReplace(hashPath, hashQuery={}){
  const router = Router();
  router[changeMissSwitcher]();
  let url = joinHash(hashPath, hashQuery);
  // log('replace', url)
  location.replace(url);
} 
/* 切换路由-history-forward 
*/
export function historyForward(){
  const router = Router();
  router[changeMissSwitcher]();
  window.history.forward();
} 
/* 切换路由-history-back 
*/
export function historyBack(){
  const router = Router();
  router[changeMissSwitcher]();
  window.history.back();
} 
/* 切换路由-history-go 
*/
export function historyGo(goNum){
  const router = Router();
  router[changeMissSwitcher]();
  goNum = goNum * 1; 
  if ( !isNumberValue(goNum) ) { return console.warn('非数值参数'); }
  if ( !history || !history.go ) { return console.warn('功能不支持'); }
  
  history.go( goNum );
} 

/* 函数组件 路由包装器 
  可定制化 路由函数组件的入参, 进行路由函数组件个性化配置 
*/
// 定制同步路由函数组件 
export function DecorateSync(cpntsMap){
  // 作为一修饰器函数 
  return function decorate(targetCpntFunc){
    // 作为一路由加载函数: 提供路由信息参数 
    return function routeLoad(oldRoute, newRoute){
      // 作为一函数组件: 提供组件通用参数 
      function PageCpnt(props, context){
        return targetCpntFunc(
          props, 
          context, 
          cpntsMap
        );
      };
      PageCpnt.scopeName = targetCpntFunc.scopeName || targetCpntFunc.name;
      return PageCpnt;
    };
  } 
} 
// 定制同步路由函数组件的默认配置 
export function DecorateSyncMore(PageCpntWrap){
  return function decorate(targetCpntFunc){
    return function routeLoad(oldRoute, newRoute){
      const PageCpnt = PageCpntWrap(targetCpntFunc, oldRoute, newRoute);
      // function PageCpnt(props, context){
      //   return (
      //     <div className="decorate_page">
      //       <FloatCatalog />
      // 
      //       { targetCpntFunc(
      //         props, 
      //         context, 
      //         cpntsMap
      //       ) }
      //     </div>
      //   )
      // };
      PageCpnt.scopeName = targetCpntFunc.scopeName || targetCpntFunc.name;
      return PageCpnt;
    };
  } 
} 
// 定制懒加载路由函数组件 
export function DecorateAsync(cpntsMap){
  return function decorate(pmsFunc){
    return function routeLoad(oldRoute, newRoute){
      return pmsFunc().then((md)=>{
        function PageCpnt(props, context){
          return md.default(
            props, 
            context, 
            cpntsMap
          );
        }
        PageCpnt.scopeName = md.default.scopeName || md.default.name;
        return PageCpnt;
      })
    };
  } 
} 
/* 定义默认的 组件配置&组件布局 */
// 定制懒加载路由函数组件的默认配置 
export function DecorateAsyncMore(PageCpntWrap){
  return function decorate(pmsFunc){
    return function routeLoad(oldRoute, newRoute){
      return pmsFunc().then((md)=>{
        const PageCpnt = PageCpntWrap(md.default, oldRoute, newRoute);
        // function PageCpnt(props, context){
        //   return (
        //     <div className="decorate_page">
        //       <FloatCatalog />
        // 
        //       { md.default(
        //         props, 
        //         context, 
        //         cpntsMap
        //       ) }
        //     </div>
        //   )
        // };
        PageCpnt.scopeName = md.default.scopeName || md.default.name;
        return PageCpnt;
      })
    };
  } 
} 
