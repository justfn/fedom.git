/* hashchange 路由切换处理
*/

// 防止多次初始化 
let is_init_switcher = false; // 初始化开关 
// 路由变动监听集合 
let listener_list = []; 



/* 路由切换 
*/
function hashchangeListener(evt, option){
  // console.log(location.hash);
  // console.log(evt);
  // evt.oldURL: "http://0.0.0.0:9000/#/home"
  // evt.newURL: "http://0.0.0.0:9000/#/tic_tac_toe"
  
  listener_list.forEach(fn=>fn(evt, option))
} 

export function initHashChange(hashchangeRun){
  if (is_init_switcher) { return ; }
  is_init_switcher = true;
  
  window.addEventListener("hashchange", (evt)=>{
    hashchangeRun(evt, (option)=>{
      hashchangeListener(evt, option)
    })
  });
  // 初始执行 
  let evt = {
    newURL: window.location.href, 
    isInitRun: true,
  }
  hashchangeRun( evt, (option)=>{
    hashchangeListener(evt, option)
  });
} 

/* 监听路由切换 
*/
function onHashChange(listener){
  listener_list.push(listener);
} 
export default onHashChange;
