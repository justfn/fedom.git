import { 
  parseURL, 
} from "./routerUtils.js";

const is_cache_key = Symbol('is_cache_key');

/* 路由对象 */
class Route {
  constructor(fullUrl, routeMap={}, urlParams=null){ 
    fullUrl = fullUrl || location.href;
    const {
      pathFull,
      pathName,
      pathQryStr,
      pathQuery,
      hashFull,
      hashPath,
      hashQryStr,
      hashQuery,
    } = urlParams || parseURL(fullUrl);
    this.fullUrl = fullUrl;
    this.pathFull = pathFull;
    this.pathName = pathName;
    this.pathQryStr = pathQryStr;
    this.pathQuery = pathQuery;
    this.hashFull = hashFull;
    this.hashPath = hashPath;
    this.hashQryStr = hashQryStr;
    this.hashQuery = hashQuery;
    this.option = routeMap[hashPath] || null;
    if ( this.option ) {
      this[is_cache_key] = this.option.isCache;
    }
  }
  
  // 动态控制-路由页面是否缓存 
  set isCache(bol){
    bol = !!bol;
    this[is_cache_key] = bol;
    return bol;
  }
  get isCache(){ return this[is_cache_key]; }
}

export default Route;
