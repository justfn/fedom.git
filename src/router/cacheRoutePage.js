/* 页面缓存处理 
*/
import { 
  isFunctionValue, 
} from "../utils/judge.js";


let hashFull_pageElem_map = {
  // <hashFull>: <Element>
}

/* 保存需缓存的页面 
*/
export function setCachePage(routeVal, pageElem=null){
  if (!routeVal) { return ; }   
  
  let routeOption = routeVal.option;
  if ( !routeOption ) { return ; }
  if ( !routeOption.component ) { return ; }
  
  let hashFull = routeVal.hashFull;
  if ( routeVal.isCache ) {
    hashFull_pageElem_map[ hashFull ] = pageElem;
    let alias = routeOption.alias;
    if (alias) { 
      hashFull_pageElem_map[ alias ] = pageElem; 
    }
  }
  else {
    hashFull_pageElem_map[ hashFull ] = null;
  }
} 

/* 获取已缓存的页面 
*/
export function getCachePage(routeVal){
  let hashFullPage = hashFull_pageElem_map[ routeVal.hashFull ];
  return hashFullPage;
} 


