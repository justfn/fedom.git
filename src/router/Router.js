/* 路由功能 
*/
import { 
  isPromiseValue, 
  isFDComponent,
} from "../utils/judge.js";
import { 
  hct_types,
  parseURL,
  formatRoutes, 
  hashPush, 
  hashReplace, 
  historyForward, 
  historyBack, 
  historyGo, 
} from "./routerUtils.js";
import Route from "./Route.js";
import { 
  updateActiveHash, 
  initActiveList,
  // updateCachedList, 
} from "../featrues/Component/activedCpnts.js";
import render from "../compiler/render.js";
import { 
  initHashChange, 
} from "./onHashChange.js";
import {
  setCachePage,
  getCachePage,
} from "./cacheRoutePage.js";

/* 路由对象  
*/
const route_list =  Symbol('route_list');
const route_map =  Symbol('route_map');
const render_node_wrap =  Symbol('render_node_wrap');
const before_change_each =  Symbol('before_change_each');
const after_change_each =  Symbol('after_change_each');
const hash_router_change =  Symbol('hash_router_change');
const old_route =  Symbol('old_route');
const new_route =  Symbol('new_route');
const miss_change_switcher =  Symbol('miss_change_switcher');
const back_until_url =  Symbol('back_until_url');
const back_until_num =  Symbol('back_until_num');
const back_default_url =  Symbol('back_default_url');
const back_until_judge =  Symbol('back_until_judge');
const back_until_finish =  Symbol('back_until_finish');
const change_miss_switcher =  Symbol('change_miss_switcher');

let routerInstance = null;
class Router {
  constructor( routeOptions={} ){ 
    const {
      routes = [
        // {
        //   path: '',
        //   alias: '',
        //   redirect: '',
        //   component: <cpt>,
        //   isCache: <bol>,
        //   extra: {}, // 自定义的额外数据 
        //   children: [
        //     {
        //       path: '', // 降低复杂度,是否以 / 开头都无区别
        //       ...
        //     },
        //     ...
        //   ],
        // },
      ],
      root = document.body,
      beforeEach = (v=>v),
      afterEach = (v=>v),
    } = routeOptions;
    const {
      routeMap,
      routeList,
    } = formatRoutes(routes);
    // console.log( 'routeMap', routeMap );
    // console.log( 'routeList', routeList );
    
    // 路由列表: 配置的所有路由集合
    this[route_list] = routeList;
    // 路由Map
    this[route_map] = routeMap;
    this[render_node_wrap] = root; 
    this[before_change_each] = beforeEach;
    this[after_change_each] = afterEach;
    this[old_route] = null;
    this[new_route] = null;
    
    initHashChange(this[hash_router_change]);
  }
  
  // 切换路由响应开关: 控制是否响应路由变化 
  [change_miss_switcher] = (bol=false)=>{
    this[miss_change_switcher] = bol;
  }
  // 路由切换 
  [hash_router_change] = (evt, hash_change_time)=>{
    // console.log(' hash change >>>>>>>>>>> 0');
    this[back_until_judge](evt.newURL);
    if (this[miss_change_switcher]) { return ; }
    // console.log(' hash change >>>>>>>>>>> 1');
    hash_change_time = hash_change_time || (v=>v);
    
    // 解析路由参数: 
    if ( this[new_route] ) { 
      this[old_route] = this[new_route]; 
    }
    else { 
      this[old_route] = new Route(evt.oldURL, this[route_map]); 
    }
    this[new_route] = new Route(evt.newURL, this[route_map]);
    // console.log( this[old_route], this[new_route] );
    
    // 未进入路由 1: hash路由错误 
    if (!this[new_route].hashPath) {
      console.log('hash路径错误:', this[new_route]);
      let firstRouteItm = this[route_list][0];
      if (!firstRouteItm) {
        console.warn('请配置路由');
        return ;
      }
      hashReplace(firstRouteItm.path);
      return ;
    }
    // 未进入路由 2: 不允许跳转 
    let isGo = this[before_change_each](
      this[old_route], 
      this[new_route]
    ) ?? true;
    if (!isGo) { 
      console.log('禁止访问的路由', this[old_route], this[new_route]);
      hashReplace(this[old_route].hashPath, this[old_route].hashQuery);
      return ; 
    }
    // 未进入路由 3: 路由重定向 
    let pathOption = this[route_map][this[new_route].hashPath] ?? {};
    if (pathOption.redirect) {
      hashReplace(pathOption.redirect, this[new_route].hashQuery);
      return ;
    }
    // 未进入路由 4: 未指定需渲染的页面组件 
    if (!pathOption.component) { 
      console.warn('render error: 未指定页面组件', pathOption);
      hashReplace(this[old_route].hashPath, this[old_route].hashQuery);
      return; 
    }
    
    setCachePage(this[old_route], this[render_node_wrap].firstElementChild);
    hash_change_time({
      init: !!evt.isInitRun,
      type: hct_types.change_start,
      oldRoute: this[old_route], 
      newRoute: this[new_route], 
    });
    updateActiveHash( this[new_route] );
    
    // 使用缓存页面 
    let cachedPageNode = getCachePage(this[new_route]); 
    if (cachedPageNode) { 
      // updateCachedList( this[new_route] );
      let isExit = [...this[render_node_wrap].childNodes].some( itm=>{
        return itm===cachedPageNode;
      })
      // 不重复渲染相同DOM 
      if (isExit) { 
        hash_change_time({
          init: !!evt.isInitRun,
          type: hct_types.cache_same,
          oldRoute: this[old_route], 
          newRoute: this[new_route], 
        });
        console.log(' --- cache page same --- ');
        return; 
      }
      
      this[render_node_wrap].innerHTML = '';
      hash_change_time({
        init: !!evt.isInitRun,
        type: hct_types.cache_show,
        oldRoute: this[old_route], 
        newRoute: this[new_route], 
      });
      render( cachedPageNode, this[render_node_wrap] );
      this[after_change_each](this[old_route], this[new_route]);
      console.log(' --- cache page render --- ');
      return ;
    }
    
    // 渲染指定页面组件 
    let routeLoaded = pathOption.component(
      this[old_route], 
      this[new_route]
    );
    let routePms = Promise.resolve(routeLoaded);
    if ( isPromiseValue(routeLoaded) ) { 
      // console.log('异步组件');
      routePms = routeLoaded.then((md={})=>{
        if (isFDComponent(md)) { return md; }
        return md.default;
      }) 
    }
    
    initActiveList();
    
    routePms.then(RouteCpnt=>{
      this[render_node_wrap].innerHTML = '';
      hash_change_time({
        init: !!evt.isInitRun,
        type: hct_types.render_start,
        oldRoute: this[old_route], 
        newRoute: this[new_route], 
      });
      // console.log(' route_render_start ');
      try {
        render( 
          <RouteCpnt 
            oldRoute={this[old_route]}
            newRoute={this[new_route]}
            push={hashPush}
            replace={hashReplace}
            go={historyGo}
            forward={historyForward}
            back={historyBack}
          />, 
          this[render_node_wrap] 
        );
        hash_change_time({
          init: !!evt.isInitRun,
          type: hct_types.render_end,
          oldRoute: this[old_route], 
          newRoute: this[new_route], 
        });
        // console.log(' route_render_end ');
        this[after_change_each](this[old_route], this[new_route]);
      } 
      catch (err) {
        // console.log(' route_render_error ');
        hash_change_time({
          init: !!evt.isInitRun,
          type: hct_types.render_error,
          oldRoute: this[old_route], 
          newRoute: this[new_route], 
        });
      } 
    })
    .catch((err)=>{
      console.error(err);
      return Promise.reject(err);
    })
  }
  
  /* -------------------------------------------------------- APIs */
  // 获取之前的路由信息 
  get getOld(){
    return { ...this[old_route], };
  }
  // 获取当前的路由信息 
  get getNew(){
    return { ...this[new_route], };
  }
  // 获取所有路由信息 
  get routes(){
    // if (isOrgin) { return [...this[route_list]];  }
    
    // todo 待优化 
    return JSON.parse(JSON.stringify( this[route_list] ));
  }
  
  /* 切换路由-回退到指定路由,若不存在则到指定路由,否则直到最开始页面 
  */
  backUntillTo = (fullUrl, dftUrl)=>{
    this[change_miss_switcher](true);
    this[back_until_url] = fullUrl;
    this[back_until_num] = history.length;
    this[back_default_url] = dftUrl;
    window.history.back();
  }
  [back_until_url] = '';
  [back_until_num] = 1;
  [back_default_url] = '';
  [back_until_judge] = (url)=>{
    if (!this[back_until_url]) { return ; }
    console.log( this[back_until_num] );
    
    // ? 
    if ( this[back_until_num]<4 ) {
      window.location.replace(this[back_default_url]);
      this[back_until_finish]();
      return ;
    }
    
    if ( this[back_until_url]!==url ) {
      window.history.back();
      this[back_until_num] -= 1;
      return ;
    }
    
    this[back_until_finish]();
  }
  [back_until_finish] = ()=>{
    this[change_miss_switcher](false);
    this[back_until_url] = '';
  }
  
}
function getRouter(options={}){
  if (routerInstance) { return routerInstance; }
  
  routerInstance = new Router(options);
  return routerInstance;
};
getRouter.push = (hashPath, hashQuery)=>{
  return hashPush(hashPath, hashQuery);
}
getRouter.replace = (hashPath, hashQuery)=>{
  return hashReplace(hashPath, hashQuery);
}
getRouter.go = (stepNum)=>{
  return historyGo(stepNum);
}
getRouter.forward = ()=>{
  return historyForward();
}
getRouter.back = ()=>{
  return historyBack();
}
getRouter.parseURL = (url)=>{
  return parseURL(url);
}

export const changeMissSwitcher = change_miss_switcher;
export default getRouter;

