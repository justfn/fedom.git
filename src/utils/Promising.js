/* 进行中的Promise 
*/


/* Promise增强版: 对外提供了可自触发状态变化的接口方法 
*/
export class PromisePlus extends Promise {
  constructor(callback){
    callback = callback || ((rs, rj)=>null);
    let _resolve = null;
    let _reject = null;
    super((resolve, reject)=>{
      _resolve = resolve;
      _reject = reject;
      return callback(resolve, reject)
    });
    this.resolve = _resolve;
    this.reject = _reject;
  }
}


function getPromisePlus(){
  // let _resolve = null; 
  // let _reject = null; 
  // let pms = new Async((resolve, reject)=>{
  //   _resolve = (val)=>{ setTimeout(()=>{ resolve(val) }) };
  //   // _resolve = resolve;
  //   _reject = reject;
  // });
  // pms.resolve = _resolve;
  // pms.reject = _reject;
  return new PromisePlus();
} 
export default getPromisePlus;
