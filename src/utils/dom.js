/* ** dom 操作封装 */


// 解析富文本 提供插入富文本的能力 
export function parseRichText(htmlStr){
  let div = document.createElement("div");
  div.innerHTML = htmlStr;
  return [...div.childNodes];
} 

// 在指定节点前插入节点 
export function insertAfter(parentNode, targetNode, flagNode=null){
  if (!parentNode) { return ; }
  if (!targetNode) { return ; }
  
  if (flagNode) { flagNode = flagNode.nextSibling; }
  parentNode.insertBefore(targetNode, flagNode);
} 


// 设置节点属性 
// 简化操作, 待优化, 
export function setElemAttr(elem, key, val){
  if (!elem) { return ; }
  
  try {
    elem.setAttribute(key, val);
    elem[key] = val;
  } 
  catch (e) {
  } 
} 

