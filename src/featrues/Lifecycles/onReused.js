/* ** 使用缓存时 
*/
import {
  isComponentValue, 
  isFunctionValue, 
  isArrayValue, 
} from "../../utils/judge.js";
import { 
  hct_types, 
} from "../../router/routerUtils.js";
import onHashChange from "../../router/onHashChange.js";
import { 
  getActivedList, 
} from "../Component/activedCpnts.js";
import { 
  onReusedFns, 
} from "../Component/Component.js";



// 执行组件的 cache 生命周期函数 
function runCpntReused(fdNode, ...args){
  if (!fdNode) { return ; }
  let context = fdNode.context;
  if ( !context ) { return ; }
  
  if ( isFunctionValue(context.onReused) ) {
    context.$mounted.then((elem)=>{
      context.onReused(elem, ...args);
    })
  }
  if ( isArrayValue(context[onReusedFns]) ) {
    context.$mounted.then((elem)=>{
      context[onReusedFns].forEach((callback)=>{
        callback(elem, ...args);
      })
    })
  }
} 

// 监听hash变化 
onHashChange((evt, option)=>{
  if (option.init) { return ; }
  let isDo = [ 
    hct_types.cache_show,
    hct_types.cache_same,
  ].includes(option.type)
  if (!isDo) { return ; }
  
  let list = getActivedList(option.newRoute);
  // console.log( 'onReused getactivedlist', list );
  list.forEach(fNd=>{
    runCpntReused(fNd); 
  })
})

// 监听指定组件的 cached 生命周期
function onReused(context, callback){
  if ( !isComponentValue(context) ) { 
    return console.error('#fd onReused context error'); 
  }
  if ( !isFunctionValue(callback) ) { 
    return console.error('#fd onReused callback error'); 
  }
  if ( !isArrayValue(context[onReusedFns]) ) { 
    return console.error('#fd onReused error'); 
  }
  
  context[onReusedFns].push((...args)=>{
    callback(...args);
  })
} 
export default onReused;

