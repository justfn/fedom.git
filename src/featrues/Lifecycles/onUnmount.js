/* ** 卸载前
*/
import {
  isComponentValue, 
  isFunctionValue, 
  isArrayValue, 
  isFDComponent, 
} from "../../utils/judge.js";
import { 
  hct_types,
  parseURL,
} from "../../router/routerUtils.js";
import onHashChange from "../../router/onHashChange.js";
import { 
  onUnmountFns, 
} from "../Component/Component.js";
import { 
  getActivedList, 
} from "../Component/activedCpnts.js";



/* ** 执行组件卸载生命周期绑定的事件  
方法二: 
1: 动态组件切换时调用 
2: 路由切换, 渲染时收集页面级组件,切换前调用 
*/
export function runCpntRemove(fdNode, ...args){
  if (!fdNode) { return ; }
  let context = fdNode.context;
  if (!context) { return ; }
  
  if ( isFunctionValue(context.onUnmount) ) {
    context.onUnmount(...args);
  }
  if ( isArrayValue(context[onUnmountFns]) ) {
    context[onUnmountFns].forEach((callback)=>{
      callback(...args);
    })
  }
} 

// 监听hash变化 
onHashChange((evt, option)=>{
  // console.log( 'unmount ', evt, option);
  if (option.init) { return ; }
  let isDo = [
    hct_types.change_start,
    // hct_types.cache_show,
    // hct_types.cache_same,
  ].includes(option.type)
  if (!isDo) { return ; }
  
  let list = getActivedList(option.oldRoute);
  // console.log(' onUnmount getactivedlist');
  // console.log( option.oldRoute.hashFull );
  // console.log( list );
  list.forEach(fNd=>{
    runCpntRemove(fNd); 
  })
})
// 监听页面刷新 
window.addEventListener("beforeunload", (evt)=>{
  let routeParams = parseURL();
  let list = getActivedList(routeParams);
  // console.log(' beforeunload ========= ', list );
  list.forEach(fNd=>{
    runCpntRemove(fNd, {
      event: evt, 
    }); 
  })
})

// 监听指定组件的 unmount 生命周期 
function onUnmount(context, callback){
  if ( !isComponentValue(context) ) { 
    return console.error('#fd onUnmount context error'); 
  }
  if ( !isFunctionValue(callback) ) { 
    return console.error('#fd onUnmount callback error'); 
  }
  if ( !isArrayValue(context[onUnmountFns]) ) { 
    return console.error('#fd onUnmount error'); 
  }
  
  context[onUnmountFns].push((...args)=>{
    callback(...args);
  })
}
export default onUnmount;


/* ** 方法一: 监听dom变动 -------------------------------------------------------
// function observe(fdNode){
//   // 非组件节点不处理 
//   if ( isFDComponent(fdNode.tagName) ) { return ; }
//   // 组件防重处理 
//   if (fdNode.children.length>0) { return ; } 
// 
//   setTimeout(()=>{
// 
//     // 定义监听回调 
//     let obCallback = (mutations,observer)=>{
//       mutations.forEach((mutation)=>{
//         console.log(mutation);
//         console.log( mutation.removedNodes );
//       })
//     }
//     // 定义变动观察器 
//     let mutation = new MutationObserver(obCallback);
//     console.log( fdNode );
//     // 观察目标节点 
//     mutation.observe(fdNode.realNode.parentNode, {
//       // characterData: false,
//       // attributes: false,
//       childList: true,
//       // subtree: false,
//       // attributeOldValue: false,
//       // characterDataOldValue: false,
//       // attributeFilter: [],
//     })
// 
//   })
// } 
*/


