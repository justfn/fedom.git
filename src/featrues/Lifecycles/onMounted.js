/* 组件渲染后 
todo 
*/
import {
  isFunctionValue, 
  isArrayValue, 
} from "../../utils/judge.js";
import { 
  onMountedFns, 
} from "../Component/Component.js";

// 执行组件渲染完毕对应绑定的事件 
export function runCpntMounted(fdNode, ...mountArgs){
  if (!fdNode) { return ; }
  let context = fdNode.context;
  if (!context) { return ; }
  
  if ( isFunctionValue(context.onMounted) ) {
    context.$mounted.then((elem)=>{
      context.onMounted(elem, ...mountArgs);
    })
  }
  if ( isArrayValue(context[onMountedFns]) ) {
    context.$mounted.then((elem)=>{
      context[onMountedFns].forEach((callback)=>{
        callback(elem, ...mountArgs);
      })
    })
  }
} 

// 监听组件渲染完毕事件
function onMounted(context, callback){
  if ( !isFunctionValue(callback) ) { 
    return console.error('#fd onMounted callback error'); 
  }
  if ( !isArrayValue(context[onMountedFns]) ) { 
    return console.error('#fd onMounted error'); 
  }
  
  context[onMountedFns].push((...args)=>{
    callback(...args);
  })
} 
export default onMounted;



