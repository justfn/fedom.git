/* ** 数组专用  
todo: 增加数组所有方法
*/
import {
  isNumberValue, 
  isArrayValue, 
  isFunctionValue, 
  isEmptyValue, 
} from "../../utils/judge.js";
import VaryValue, {
  Vary, 
} from "./VaryValue.js";

class ListId {
  constructor(idNum){
    this.value = idNum;
  }
}
const list_in_set_fns_key = Symbol('list_in_set_fns');
const list_rm_set_fns_key = Symbol('list_rm_set_fns');
const init_list = Symbol('init_list');
const splice_method = Symbol('splice_method');
const is_itm_id = Symbol('is_itm_id');
const find_idx = Symbol('find_idx');
const check_idx = Symbol('check_idx');
const add_list_in = Symbol('add_list_in');
const add_list_rm = Symbol('add_list_rm');
const itm_trim_fn = Symbol('itm_trim_fn');
const store_list = Symbol('store_list');
const init_id_list = Symbol('init_id_list');
export class ListVary extends Vary {
  constructor(val, itmTrimFn){
    function trimFn(lst){
      this[init_id_list] = [];
      return lst.map((itm, idx)=>{
        let itmId = new ListId(idx);
        this[init_id_list].push(itmId);
        return itmTrimFn(itm, idx, itmId, lst);
      });
    } 
    super(val, trimFn);
    
    this[itm_trim_fn] = itmTrimFn;
    
    // VaryList 备份&跟踪 
    this[store_list] = [
      // {
      //   $$: <val>,
      //   id: new ListId(idx),
      // }
    ]; 
    this[init_list]();
  }
  
  /* --------------------------------------------------------- DATAs  */
  [list_in_set_fns_key] = [];
  [list_rm_set_fns_key] = [];
  
  /* --------------------------------------------------------- KITs  */
  arr_item_id_num = -99;
  [init_list] = ()=>{
    this[store_list] = this.$$.map((itm,idx)=>{
      this.arr_item_id_num = idx;
      return {
        $$: itm, 
        id: this[init_id_list][idx],
      }
    })
  }
  [splice_method] = (begin, num, list=[], updateIdList=[], trimList=[])=>{
    let bakList = list.map((itm,idx)=>{
      return {
        $$: itm, 
        id: updateIdList[idx],
      }
    })
    
    this.$$.splice(begin, num, ...list);
    this.get(true).splice(begin, num, ...trimList);
    this[store_list].splice(begin, num, ...bakList);
  }
  [is_itm_id] = (val)=>{
    if ( !val ) { return false; }
    if ( val instanceof ListId ) { return true; }
    
    return false;
  }
  [find_idx] = (id)=>{
    let idx = this[store_list].findIndex( 
      itm=>itm.id===id 
    ); 
    if (idx===-1) { throw 'fd VaryList id 错误'; }
    
    return idx;
  }
  [check_idx] = (idx)=>{
    if (isEmptyValue(idx)) { idx = 0 } 
    if (!isNumberValue(idx)) { throw 'fd VaryList idx 错误'; }
    if (idx>this.$$.length) { idx = this.$$.length }
    if (idx<0) { idx = 0 }
    
    return idx;
  }
  /* --------------------------------------------------------- APIs  */
  // 指定位置起,插入若干项 
  $insert = (insertRunOrItmidOrIdx, lst)=>{
    let idx = insertRunOrItmidOrIdx;
    if (isFunctionValue(insertRunOrItmidOrIdx)) {
      let result = insertRunOrItmidOrIdx([...this.$$]);
      idx = result[0];
      lst = result[1];
    }
    else if ( this[is_itm_id](insertRunOrItmidOrIdx)) {
      idx = this[find_idx](insertRunOrItmidOrIdx)
    }
    idx = this[check_idx](idx);
    
    let updateIdList = [];
    let trimList = lst.map((itm, idx1)=>{
      this.arr_item_id_num++;
      let itmId = new ListId( this.arr_item_id_num );
      updateIdList.push(itmId);
      return this[itm_trim_fn]( itm, idx, itmId, this.$$);
    })
    this[list_in_set_fns_key].forEach((listInSetItm)=>{
      listInSetItm({
        index: idx,
        list: trimList,
      });
    })
    this[splice_method](idx, 0, lst, updateIdList, trimList);
    return Promise.resolve();
  }
  // 指定位置起,删除若干项
  $remove = (removeRunOrItmidOrIdx, len=1)=>{
    if ( !this[store_list].length ) { return Promise.resolve(); }
    if ( len===0 ) { return Promise.resolve(); }
    
    let idx = removeRunOrItmidOrIdx;
    if (isFunctionValue(removeRunOrItmidOrIdx)) {
      idx = removeRunOrItmidOrIdx([...this.$$]);
    }
    else if (this[is_itm_id](removeRunOrItmidOrIdx)) {
      // console.log( removeRunOrItmidOrIdx, this[store_list] );
      idx = this[find_idx](removeRunOrItmidOrIdx)
    }
    idx = this[check_idx](idx);
    
    this[list_rm_set_fns_key].forEach((listRmSetItm)=>{
      listRmSetItm({
        index: idx,
      });
    })
    this[splice_method](idx, len);
    return Promise.resolve();
  }
  // 更新指定位置项的值 
  $update = (updateRunOrItmidOrIdx, val)=>{
    let idx = updateRunOrItmidOrIdx;
    if (isFunctionValue(updateRunOrItmidOrIdx)) {
      let result = updateRunOrItmidOrIdx([...this.$$]);
      idx = result[0];
      val = result[1];
    }
    else if (this[is_itm_id](updateRunOrItmidOrIdx)) {
      idx = this[find_idx](updateRunOrItmidOrIdx)
    }
    idx = this[check_idx](idx);
    
    return this.$remove( idx )
    .then(()=>{
      return this.$insert( idx, [val] );
    })
  }
  // 截取指定区间的项
  $slice = (startIdxOrId, endIdxOrId)=>{
    let startIdx = startIdxOrId;
    let endIdx = endIdxOrId;
    if (this[is_itm_id](startIdxOrId)) { 
      startIdx = this[find_idx](startIdxOrId); 
    }
    if (this[is_itm_id](endIdxOrId)) { 
      endIdx = this[find_idx](endIdxOrId); 
    }
    startIdx = this[check_idx](startIdx);
    endIdx = this[check_idx](endIdx);
    if (startIdx>=endIdx) { 
      throw 'fd VaryList slice start is bigger than end' 
    }
    
    let promiseList = [];
    let lenEnd = this.$$.length - endIdx;
    if (lenEnd>0) {
      Array(lenEnd).fill('').forEach((itm,idx)=>{
        promiseList.push( this.$remove(endIdx) )
      })
    }
    let lenStart = startIdx - 0; 
    if (lenStart>0) {
      Array(lenStart).fill('').forEach((itm,idx)=>{
        promiseList.push( this.$remove(0) )
      })
    }
    
    return Promise.all( promiseList )
  }
  // 
  $unshift = (...list)=>{
    return this.$insert(0, list)
    .then(()=>{
      return this.$$.length;
    })
  }
  // 
  $push = (...list)=>{
    return this.$insert(this.$$.length, list)
    .then(()=>{
      return this.$$.length;
    })
  }
  // 
  $shift = ()=>{
    if (this.$$.length===0) { return Promise.resolve(); }
    
    let firstItm = this.$$[0];
    return this.$remove(0).then(()=>{
      return firstItm;
    })
  }
  // 
  $pop = ()=>{
    if (this.$$.length===0) { return Promise.resolve(); }
    
    let lastItm = this.$$[this.$$.length-1];
    return this.$remove(this.$$.length-1)
    .then(()=>{
      return lastItm;
    })
  }
  // 
  $map = (forEachRun)=>{
    forEachRun = forEachRun || function(){ };
    
    return this[store_list].map((itm, idx, list)=>{
      return forEachRun(itm.$$, idx, itm.id, list)
    })
  }
  
  // 全量更新 
  $set = (list)=>{
    return this.$remove(0, this.$$.length)
    .then(()=>{
      return this.$insert(0, list)
    })
  }
  // 重写 set 方法 
  set = (...args)=>{
    return super.set(...args).then(()=>{
      return this[init_list]();
    })
  }
  
  // 收集更新-插入 
  [add_list_in] = (listInSetRun)=>{
    this[list_in_set_fns_key].push(({index, list, id})=>{
      listInSetRun({
        index,
        list, 
      }) 
    });
  } 
  // 收集更新-删除
  [add_list_rm] = (listRemoveRun)=>{
    this[list_rm_set_fns_key].push(({index})=>{
      listRemoveRun({
        index,
      }) 
    });
  } 
}


function VaryList(list, itmTrimFn){
  if (!isArrayValue(list)) { 
    let msg = 'fd: not list data for VaryList';
    console.error(msg);
    return new Error(msg);
  }
  
  itmTrimFn = itmTrimFn || function(val, idx, id, list){ 
    return val; 
  }
  
  const varyedList = new ListVary(list, itmTrimFn);
  
  return varyedList;
} 

export const addListIn = add_list_in;
export const addListRm = add_list_rm;
export default VaryList; 

