

import diffValue from "../../utils/diffValue.js";
import {
  isVaryValue,
  isArrayValue,
  isEmptyValue,
  isNumberValue, 
} from "../../utils/judge.js";


/** 可变量对象 
* @author  fsl 
* @time    时间值 
* -----------------------------
* @import   引入方式说明 
* @example  使用方式说明 
* -----------------------------
* @detail  
- 01 详细说明1 
* -----------------------------
* @todo  
- 1 代办事项1 
*/

/* 可变量类 
*/
let use_vary_num_id = 0; 
const symbol_1 = Symbol('symbol_1'); // 唯一标识1 
const set_fns_key = Symbol('set_fns_key');
const watch_fns_key = Symbol('watch_fns_key');
const mounted_fns_key = Symbol('mounted_fns_key');
const push_set_key = Symbol('push_set_key');
const push_mounted_key = Symbol('push_mounted_key');
const is_alive = Symbol('is_alive');
const real_val = Symbol('real_val');
const trim_val_fn = Symbol('trim_val_fn');
const trimed_val = Symbol('trimed_val');
const trimed_next_val = Symbol('trimed_next_val');
export class Vary {
  constructor(val, trimFn) {
    this._num_id = use_vary_num_id++;
    
    this[is_alive] = true; 
    this[real_val] = val; 
    this[trim_val_fn] =  trimFn ?? (v=>v); // 整理成最终返回值 
    this[trimed_val] = this[trim_val_fn](val);
    // 缓存下一次格式化的值,避免多次执行'trim_val_fn'函数  
    this[trimed_next_val] = symbol_1; 
  }
  
  /* --------------------------------------------------------- DATAs  */
  [set_fns_key] = [];
  [watch_fns_key] = [];
  [mounted_fns_key] = [];
  /* --------------------------------------------------------- APIs  */
  // 取值 
  get(isTrimed=false){ 
    if (isTrimed) { return this[trimed_val]; }
    
    return this[real_val]; 
  }
  // 设值  
  // todo: 增加入参为具体值的场景 
  set(setHandle, isLazy=true){
    if (!this[is_alive]) { return Promise.reject('sleeping'); }
    
    let pre_v = this.get();
    let pre_v_t = this.get(true);
    let nxt_v = null;
    if (!isLazy) { 
      // todo; diff value 
      this[set_fns_key].forEach(setFn=>{ 
        nxt_v = setFn(setHandle, isLazy).nextValue; 
      });
    }
    else {
      nxt_v = setHandle(pre_v, pre_v_t); 
      // diff nxt_v pre_v 
      if (diffValue(pre_v, nxt_v)) { return Promise.resolve(); }
      
      if (nxt_v===undefined) { nxt_v = pre_v }
      this[trimed_next_val] = this[trim_val_fn](nxt_v);
      this[set_fns_key].forEach(setFn=>{ 
        setFn(nxt_v, isLazy); 
      });
    }
    this[real_val] = nxt_v;
    let tmpV = this[trimed_val];
    this[trimed_val] = this[trimed_next_val];
    this[trimed_next_val] = symbol_1; 
    this[watch_fns_key].forEach( watchFn=>{
      watchFn(nxt_v, pre_v, this[trimed_val], tmpV);
    })
    
    return Promise.resolve(nxt_v);
  }
  get $$(){ return this.get(); }
  set $$(val){ this.set(v=>val, true) }
  // 收集渲染后执行的函数 
  mounted = (mountedHandle)=>{
    this[mounted_fns_key].push(mountedHandle);
  }
  // 收集更新时执行的函数 
  watch = (watchHandle)=>{
    this[watch_fns_key].push((n_v, p_v, nVTrimed, pVTrimed)=>{
      watchHandle(n_v, p_v, nVTrimed, pVTrimed);
    })
  }
  // 控制开关 
  on = ()=>{ this[is_alive] = true; }
  off = ()=>{ this[is_alive] = false; }  
  kill = ()=>{
    this[is_alive] = false; 
    // todo 待优化 
    for(let key in this){
      this[key] = null; 
    };
  }
  
  /* --------------------------------------------------------- KITs  */
  // 收集更新 
  [push_set_key] = (setRun, extra)=>{
    this[set_fns_key].push((setHandle, isLazy)=>{
      let pre_v = this.get();
      let pre_v_t = this.get(true);
      let nxt_v = setHandle; 
      if (!isLazy) { 
        nxt_v = setHandle(pre_v, pre_v_t, extra); 
        if (nxt_v===undefined) { nxt_v = pre_v }
        if (this[trimed_next_val]===symbol_1) {
          this[trimed_next_val] = this[trim_val_fn](nxt_v);
        }
      }
      let updatedReturenValue = setRun({
        preTrimedValue: pre_v_t,
        nxtTrimedValue: this[trimed_next_val],
        preValue: pre_v,
        nxtValue: nxt_v,
        extra: extra,
      }) || {};
      return {
        ...extra,
        nextValue: nxt_v,
      };
    });
  }
  // 执行初始化 
  [push_mounted_key] = (...args)=>{
    this[mounted_fns_key].forEach((mountedFn,idx)=>{
      mountedFn(this.get(), this.get(true), ...args);
    })
  }
}

/* 使用可变量 
*/
const err_log1 = 'error arguments of VaryValue';
function VaryValue(val, trimFn){
  if ( isVaryValue(val) ) { con.console.error(err_log1, val); }
  
  const varyVal = new Vary(val, trimFn);
  return varyVal;
}
// 返回依赖动态值列表的动态值 
VaryValue.depend = (list, trimFn)=>{
  let v = trimFn( list.map(itm1=>itm1.$$) );
  let vv = new VaryValue( v );
  list.forEach((itm,idx)=>{
    itm.watch((newVal)=>{
      v = trimFn( list.map(itm1=>itm1.$$) )
      vv.$$ = v;
    })
  })
  return vv;
}

export const addSet = push_set_key;
export const addMounted = push_mounted_key;
export default VaryValue;

