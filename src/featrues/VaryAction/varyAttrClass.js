
import { 
  isVaryValue, 
} from "../../utils/judge.js";
import { 
  addSet, 
  addMounted, 
} from "../VaryModel/VaryValue.js";

export function varyAttrClassStr(fdNode, varyAttr){
  if (!varyAttr) { return ; }
  
  let elem = fdNode.realNode;
  let vl = varyAttr.get(true);
  varyAttr[addMounted](elem.className);
  varyAttr[addSet](({ nxtTrimedValue, })=>{
    elem.className = nxtTrimedValue;
  }, elem.className);
} 
export function varyAttrClassArr(fdNode, varyAttr){
  if (!varyAttr) { return ; }
  
  console.log('todo ');
} 
export function varyAttrClassListItm(fdNode, varyAttrItm){
  if (!isVaryValue(varyAttrItm)) { return varyAttrItm; }
  
  let elem = fdNode.realNode;
  let it = varyAttrItm.get(true);
  varyAttrItm[addMounted](elem.classList);
  varyAttrItm[addSet](({ preTrimedValue, nxtTrimedValue })=>{
    if (preTrimedValue!=='') { elem.classList.remove(preTrimedValue); }
    if (nxtTrimedValue!=='') { elem.classList.add(nxtTrimedValue); }
  }, elem.classList)
  return it;
} 


