/* ** 控制组件/元素是否存在 */
import { 
  isCommentNode, 
  isVaryValue, 
  isFDComponent,
} from "../../utils/judge.js";
import { 
  insertAfter, 
} from "../../utils/dom.js";
import { 
  runCpntMounted, 
} from "../Lifecycles/onMounted.js";
import { 
  runCpntRemove, 
} from "../Lifecycles/onUnmount.js";
import { 
  removeActiveCpnts,
  gatherActiveCpnts,
} from "../Component/activedCpnts.js";
import { 
  addSet, 
} from "../VaryModel/VaryValue.js";

const global_config = {
  keyName: 'fd_if',
}

export function cpntIsInitNone(tagName, attrs){
  let isCpnt = isFDComponent(tagName);
  if (!isCpnt) { return false; }
  
  let isDefine = global_config.keyName in attrs;
  if (!isDefine) { return false; }
  
  let val = attrs[global_config.keyName];
  if (isVaryValue(val)) { val = val.get(true); }
  if (!!val) { return false; }
  
  return true;
} 

export function initFdIfHandle(fdNode, attrs){
} 
function varyAttrVaryIf(fdNode, attrs){
  let realNode = fdNode.realNode;
  if (!(global_config.keyName in attrs)) { return ; }
  if (isCommentNode(realNode)) { return ; }
  
  // todo: 优化 
  Promise.resolve().then(()=>{
    // 一次性 控制 
    let originAttrVal = attrs[global_config.keyName];
    delete attrs[global_config.keyName];
    let flgNode = document.createComment("fedom vary if flg node");
    let parentEl = realNode.parentElement;
    parentEl.insertBefore(flgNode, realNode);
    if ( !isVaryValue(originAttrVal) ) {
      if ( !originAttrVal ) { 
        parentEl.removeChild(realNode); 
        // todo: 优化 
        setTimeout(()=>{
          runCpntRemove(fdNode);
          removeActiveCpnts(fdNode);
        })
      }
      return ;
    }
    
    // vary 控制 
    let attrVal = originAttrVal.get(true);
    if ( !attrVal ) { 
      parentEl.removeChild(realNode);
      // todo: 优化 
      setTimeout(()=>{
        runCpntRemove(fdNode);
        removeActiveCpnts(fdNode);
      }) 
    }
    originAttrVal[addSet](({ preTrimedValue, nxtTrimedValue })=>{
      if (!!nxtTrimedValue) {
        insertAfter(parentEl, realNode, flgNode);
        // parentEl.insertBefore(realNode, flgNode)
        runCpntMounted(fdNode);
        gatherActiveCpnts(fdNode);
      }
      else {
        parentEl.removeChild(realNode);
        runCpntRemove(fdNode);
        removeActiveCpnts(fdNode);
      }
    })
  })
} 
export default varyAttrVaryIf;

