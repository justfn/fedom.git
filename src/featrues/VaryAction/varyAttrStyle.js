
import {
  isVaryValue,
} from "../../utils/judge.js";
import { addSet, addMounted } from "../VaryModel/VaryValue.js";


export function varyAttrStyleStr(fdNode, varyAttr){
  if (!varyAttr) { return ; }
  
  let str = varyAttr.get(true);
  console.log('# todo');
} 
export function varyAttrStyleObj(fdNode, varyAttr){
  if (!varyAttr) { return ; }
  
  let obj = varyAttr.get(true);
  
  console.log('# todo');
} 
export function varyAttrStyleOval(fdNode, styKey, varyAttrVal ){
  if (!isVaryValue(varyAttrVal)) { return varyAttrVal; }
  
  let elem = fdNode.realNode;
  let value = varyAttrVal.get(true);
  elem.style[styKey] = value;
  varyAttrVal[addMounted]( value );
  varyAttrVal[addSet](({ nxtTrimedValue })=>{
    elem.style[styKey] = nxtTrimedValue;
  })
  return value;
} 

