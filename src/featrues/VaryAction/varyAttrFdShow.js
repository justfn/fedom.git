/* ** 控制组件/元素是否显示 */
import { 
  isCommentNode, 
  isVaryValue, 
} from "../../utils/judge.js";
import { 
  addSet, 
} from "../VaryModel/VaryValue.js";

const global_config = {
  keyName: 'fd_show',
}
function varyAttrVaryShow(fdNode, attrs){
  let {
    realNode, 
  } = fdNode;
  if (!(global_config.keyName in attrs)) { return ; }
  if (isCommentNode(realNode)) { return ; }
  
  let originAttrVal = attrs[global_config.keyName];
  delete attrs[global_config.keyName];
  // 一次性 控制 
  if ( !isVaryValue(originAttrVal) ) {
    if ( !originAttrVal ) { realNode.style.display = 'none'; }
    return ;
  }
  
  if ( !originAttrVal.get(true) ) { realNode.style.display = 'none'; }
  let orginDisplay = realNode.style.display;
  if (orginDisplay==='none') { orginDisplay = ''; }
  originAttrVal[addSet](({ preTrimedValue, nxtTrimedValue })=>{
    realNode.style.display = !!nxtTrimedValue ? orginDisplay : 'none';
  })
} 
export default varyAttrVaryShow;

