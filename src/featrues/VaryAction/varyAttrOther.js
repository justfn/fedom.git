
import { 
  isVaryValue, 
} from "../../utils/judge.js";
import { 
  setElemAttr, 
} from "../../utils/dom.js";
import { 
  addSet, 
} from "../VaryModel/VaryValue.js";


export function varyAttrOtherVal(fdNode, attrKey, varyAttr){
  if (!varyAttr) { return ; }
  
  varyAttr[addSet]((params)=>{
    let {
      nxtTrimedValue, 
    } = params;
    
    setElemAttr(fdNode.realNode, attrKey, nxtTrimedValue);
  })
} 


