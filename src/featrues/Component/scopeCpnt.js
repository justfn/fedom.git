/* 编译空间打标, 便于css隔离使用  
*/


const compiler_queue = []; 
const scope_flag = 'data-fd-scope';
const scope_field = 'scopeName';
let queue_dft_num = 0; 
let current_scope = '';
export function scopeMark(attrs){
  attrs[scope_flag] = current_scope;
  return current_scope;
} 
export function preParse( tagName ){
  let scopename = tagName[scope_field] 
    || tagName.name 
    || `component${queue_dft_num++}`;
  compiler_queue.unshift( scopename )
  current_scope = scopename;
  return current_scope;
} 
export function nxtParse(){
  compiler_queue.shift();
  current_scope = compiler_queue[0];
  return current_scope;
} 

export const scopeField = scope_field;

// 
export function getScopeName(){
  return current_scope;
} 
export function setScopeName(name){
  current_scope = name;
  return current_scope;
} 