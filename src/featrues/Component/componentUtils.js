/* 组件工具  
* -----------------------------
- 
*/
import {
  isStringValue,
  isComponent, 
  isFdNode,
} from "../../utils/judge.js";
import { 
  runCpntMounted, 
} from "../Lifecycles/onMounted.js";
import Component from "./Component.js";



/* 组件渲染 
*/
const render_fn_key = 'render';
export function renderCpnt(componentTag, props){
  let context = null;
  let realNode = null;
  let pageFdNode = null;
  try { 
    // 类组件 
    if ( isComponent(componentTag) ) { 
      context = new componentTag(props); 
      // 注意：此处又将调用 compiler 
      pageFdNode = context[render_fn_key](props);
    }
    // 函数组件 
    else {
      context = new Component(props); 
      // 注意：此处又将调用 compiler 
      pageFdNode = componentTag(props, context); 
    }
    // 简化复杂度: 组件根节点必须为标签 
    if ( !isFdNode(pageFdNode) || !isStringValue(pageFdNode.tagName)  ) {
      console.error(pageFdNode);
      throw new Error('组件根节点请使用标签包裹');
    }
    
    pageFdNode.context = context;
    realNode = pageFdNode.realNode;
    
    context.$mounted.then((root)=>{
      runCpntMounted(pageFdNode, root);
      return root;
    })
    context.$mounted.resolve(realNode);
  } 
  catch (err) {
    console.error(err);
    throw err;
  } 
  
  return pageFdNode;
  // {
  //   context, 
  //   realNode, 
  // };
} 




/* 组件更新 
* @params  fdNode  type,参数说明
* @params  context  type,参数说明
* @return  null 
* -----------------------------
- 
*/
export function updateCpnt(fdNode, context){
  fdNode.context = context;
  // fdNode.varyTag.kill();
  // fdNode.context = {}; 
} 

