/* 当前路由活动的组件集合  
*/
import { 
  isFunctionValue, 
} from "../../utils/judge.js";


// 路由组件节点映射 
let hashFull_fdNodeList_map = {
  // <hashFull>: <fdNodeList>, 
}
// 当前页完整的hash 
let current_hashFull = '';

/* 更新当前路由  
*/
export function updateActiveHash(pathParams){
  current_hashFull = pathParams.hashFull;
} 

/* 页面组件调用前执行 (全量更新) 
*/
export function initActiveList(){
  hashFull_fdNodeList_map[ current_hashFull ] = [];
} 

// 收集当前路由下编译的组件 (微调-添加) 
export function gatherActiveCpnts(fdNode){
  let list = hashFull_fdNodeList_map[ current_hashFull ];
  if (!list) { 
    hashFull_fdNodeList_map[ current_hashFull ] = []; 
    list = hashFull_fdNodeList_map[ current_hashFull ];
  }
  let isExit = list.some((itm,idx)=>{ 
    return itm===fdNode; 
  });
  if (!isExit) { list.push(fdNode); }
  
} 
// 去掉已动态卸载的组件 (微调-移除) 
export function removeActiveCpnts(fdNode){
  let list = hashFull_fdNodeList_map[ current_hashFull ];
  if (list && list.length) { 
    let index = list.findIndex((itm,idx)=>{ 
      return itm===fdNode; 
    });
    if (index!==-1) { 
      list.splice(index, 1);
      // console.log(' 0001 ', index);
      // console.log( hashFull_fdNodeList_map );
    }
  }
  
} 

// 获取当前路由下活动的组件 (获取)
export function getActivedList(routeParams={}){
  let {
    hashFull = current_hashFull,
  } = routeParams;
  // console.log( JSON.stringify( hashFull_fdNodeList_map, null, 2 )); 
  let hashFullPageList = hashFull_fdNodeList_map[hashFull];
  if (hashFullPageList && hashFullPageList.length) {
    return hashFullPageList;
  }
  
  return [];
} 



