/* 组件基础类  
  作用： 
  1 提供功能方法
  2 定义回调响应 
  3 ... 
*/
import Promising from "../../utils/Promising.js";

const on_mounted_fns = Symbol('on_mounted_fns');
const on_unmount_fns = Symbol('on_unmount_fns');
const on_reused_fns = Symbol('on_reused_fns');
class Component {
  /* --------------------------------------------------------- 生命周期 */
  // 渲染-前0 
  constructor(props={}){ 
    this.props = props;
    // 
  }
  // 渲染-前1 
  render(){ }
  // 渲染-后 
  [on_mounted_fns] = [];
  onMounted(){}
  // 使用缓存 
  [on_reused_fns] = [];
  onReused(){}
  // 卸载-前 
  [on_unmount_fns] = [];
  onUnmount(){ }
  
  /* --------------------------------------------------------- 快捷访问 */
  $mounted = Promising(); 
  refs = {};
  
}

export const onMountedFns = on_mounted_fns;
export const onUnmountFns = on_unmount_fns;
export const onReusedFns = on_reused_fns;
export default Component;
