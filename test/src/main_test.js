// 引入依赖 
// import "fedom";
import "../../dist/main.js";

// 全局样式 
import "./assets/styles/global.less";
// 路由控制 
// import router from "./router/router.js";

const {
  render, 
  VaryValue, 
} = window.$fd;


window.addEventListener("load",function(evt){
  let rootNode = document.getElementById("app");
  // render(<div className="">
  //   这是一段被渲染的文本
  // </div>, rootNode);
  
  // render('这是一段被渲染的文本', rootNode);
  
  // let divDom = document.createElement("div");
  // divDom.textContent = '这是一段被渲染的文本'
  // let result = render(divDom, rootNode);
  
  // let v_dom = VaryValue('aaa');
  // let result = render( v_dom, rootNode );
  // setTimeout(()=>v_dom.$$='bbb',3000) // 3秒后改变
  
  // let v_dom = VaryValue(<div> 这是一段被渲染的文本 </div>);
  // let result = render( v_dom, rootNode );
  // setTimeout(()=>v_dom.$$='bbb',3000) // 3秒后改变
  
  // let v_dom = VaryValue(document.createElement("input"));
  // let result = render( v_dom, rootNode );
  // setTimeout(()=>v_dom.$$='bbb',3000) // 3秒后改变
  
  // let v_dom = VaryValue( [ 
  //   'aaa',
  //   <div> 这是一段被渲染的文本 </div>, 
  //   VaryValue('ccc') 
  // ] );
  // let result = render( v_dom, rootNode );
  // setTimeout(()=>v_dom.$$='bbb',3000) // 3秒后改变
  
  // let v_val = VaryValue(0, (val)=>'已经过时间: '+ val +' s');
  // render(v_val, rootNode);
  // setInterval(()=>v_val.set(preVal=>preVal+1),1000)
  
  let v_val = VaryValue(true);
  render( [
    <div fd_if={v_val}> ⭐️ </div>,
    <div fd_show={v_val}> ⭐️ </div>,
  ], rootNode);
  setInterval(()=>v_val.set(pV=>!pV),1000)
})









