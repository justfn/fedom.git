const {
  VaryValue,
  Router,
} = window.$fd;
import "./index.less";
import FeatureTips from "../../../parts/FeatureTips/index.js";



function catalogPage(param, ctx, cpnts){
  console.log( ' page catalog ' );
  // console.log( param );
  const router = Router();
  // ctx.onMounted = ()=>{
  //   console.log(' catalog-home onMounted');
  // }
  // ctx.onUnmount = ()=>{
  //   console.log(' catalog-home onUnmount');
  // }
  // ctx.onReused = ()=>{
  //   console.log(' catalog-home onReused');
  // }

  
  return (
    <div className="catalogPage">
      <h1 children={'路由页面'} />

      {
        router.routes.map((itm,idx)=>{
          return (
            <div className="">
              <a className="linkItm" 
                href={'#'+itm.path}>
                { Array(itm.level+1).fill('').map(()=>'>')}

                { itm.path }
              </a>
            </div>
          )
        })
      }

      <FeatureTips list={[
        {
          title: '路由懒加载',
          list: [
            '该页面使用了路由懒加载, 只有首次访问到该页面时, 才会加载该页面的代码',
          ],
        },
        {
          title: '路由组件携带',
          list: [
            '通过路由处理方法, 对页面组件进行了包装, 携带了自定义的组件, 且默认渲染了浮动路由组件[FloatCatalog]',
            '携带的组件如下: ',
          ...Object.keys(cpnts),
          ],
        },
      ]}/>

    </div>
  );
} 
// catalogPage.scopeName = 'catalogPage';
export default catalogPage;


