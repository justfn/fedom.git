const {
  VaryValue, 
  Promising,
} = window.$fd
import "./index.less";
import FeatureTips from "../../parts/FeatureTips/index.js";


function homePage(props, ctx, cpnts){
  console.log(' page home');
  // console.log( 'home page', props, ctx, cpnts);
  
  return (
    <div className="homePage">
      <h1 children={'Here is home page '} />

      <FeatureTips list={[
        {
          title: '同步路由加载',
          list: [
            `首页未使用路由懒加载, 提升首页的渲染速度`,
          ],
        },
        {
          title: '相同路由也会重新渲染',
          list: [
            `不同的路径或参数, 但是都为该同一页面时( 如 #/ 到 #/home, 或者 #/ 到 #/?a=1), 页面也会重新渲染`,
            `为了减少声明周期API, 及代码复杂度`,
            `该特性目前暂未考虑支持到不重复渲染`,
          ],
        },
      ]}/>

    </div>
  );
}
export default homePage;



