const {
  VaryValue,
  onMounted,
  onUnmount,
  onReused,
} = window.$fd;
import "./sameHashPath.less";
import PartTestA from "../../../parts/PartTestA/PartTestA.js";



function sameHashPath(props, context, preRoute, currentRoute){
  // console.log( preRoute, currentRoute );
  // context.onMounted = ()=>{
  //   console.log(' same-hash-path onMounted');
  // }
  // context.onUnmount = ()=>{
  //   console.log(' same-hash-path onUnmount');
  // }
  // context.onReused = ()=>{
  //   console.log(' same-hash-path onReused');
  // }
  onMounted(context, ()=>{
    console.log(' same-hash-path onMounted 1');
  }) 
  onUnmount(context, ()=>{
    console.log(' same-hash-path onUnmount 1');
  }) 
  onReused(context, ()=>{
    console.log(' same-hash-path onReused 1');
  }) 
  
  const is_exist1 = VaryValue(true);
  const is_exist2 = VaryValue(false);
  const part_test_a3 = VaryValue(<PartTestA children={'组件3'} />)
  const part_test_a4 = VaryValue(null)
  const switchPart1 = ()=>{
    is_exist1.set((val)=>{
      return !val;
    })
  }
  const switchPart2 = ()=>{
    is_exist2.set((val)=>{
      return !val;
    })
  }
  const switchPart3 = ()=>{
    part_test_a3.set((val)=>{
      return val ? null : <PartTestA children={'组件3'} />;
    })
  }
  const switchPart4 = ()=>{
    part_test_a4.set((val)=>{
      return val ? null : <PartTestA children={'组件4'} />;
    })
  }
  
  return (
    <div className="">
      <div className="">
        相同 hash path 场景 


        <div className="">
          <button 
            children={'组件1切换'} 
            onClick={switchPart1}
          />
          <button 
            children={'组件2切换'} 
            onClick={switchPart2}
          />
          <button 
            children={'组件3切换'} 
            onClick={switchPart3}
          />
          <button 
            children={'组件4切换'} 
            onClick={switchPart4}
          />
        </div>

        <div 
          fd_if={is_exist1}
          children={'aaaa'}
        />

        <PartTestA 
          fd_if={is_exist1} 
          children={'组件1'}
        />
        <PartTestA 
          fd_if={is_exist2} 
          children={'组件2'}
        />
        { part_test_a3 }
        { part_test_a4 }
      </div>
    </div>
  );
}
export default sameHashPath;


