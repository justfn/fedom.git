const {
  VaryValue,
} = window.$fd;
import "./asyncPage.less";
import PartA from "../../../parts/PartA/PartA.js";


function asyncPage(props, context){
  
  // let pms = new Promise((resolve,reject)=>{
  //   setTimeout(()=>{
  //     resolve(
  //       <div className="">
  //         异步DOM渲染
  //       </div>
  //     )
  //   },1000)
  // })
  // return pms;
  
  let vv1 = VaryValue(0, (v)=>{
    if (v>0) {
      return (
        // <div className=""> 222 </div>
        <PartA />
      );
    }
  
    return 111;
  });
  let vv2 = VaryValue(false);
  setTimeout(()=>{
    // vv1.$$ += 1;
    vv2.set((val)=>{
      return !val;
    })
  },1000 * 2)
  
  return (
    <div className="" fd_if={vv2}>
      { vv1 }
    </div>
  );
} 
asyncPage.scopeName = 'asyncPage';
export default asyncPage;


