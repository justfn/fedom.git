const {
  Component,
  VaryValue,
} = window.$fd;
import "./upLogic.less";
import decoratorOfLayoutA from "../../../parts/LayoutA/decorator.js";
import trackMouse from "../../../logics/trackMouse.js";


@decoratorOfLayoutA 
class Page extends Component {
  constructor(props){
    super(props);
    
  }
  onMounted(){
    console.log(" >>>>>>>>>>>>>> ", 'onMounted')
    this.init();
  }
  onReused(){
    console.log(" >>>>>>>>>>>>>> ", 'onReused ')
    this.init();
  }
  onUnmount(){
    console.log('  onUnmount  ');
  }
  
  init(){
    let { x, y } = trackMouse(this);
    this.x = x;
    this.y = y;
    this.x.watch((...args)=>{
      console.log( 'watch x:', args);
    })
  }
  
  
  render(){
    console.log("000000000 x", this.x)
    
    return (
      <section className="upLogic" >
        <div>
          <span>坐标X:</span>
          <span>{ this.x }</span>
        </div>
        <div>
          <span>坐标Y:</span>
          <span>{ this.y }</span>
        </div>

        <div className="">
          该页面使用Class组件
        </div>
      </section>
    )
  };
}
export default Page;


