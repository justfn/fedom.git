const {
  VaryValue,
  onMounted,
  onUnmount,
  onReused,
} = window.$fd;
import "./sameHashCache.less";
import PartTestA from "../../../parts/PartTestA/PartTestA.js";


function sameHashCache(props, context, preRoute, currentRoute){
  // console.log( preRoute, currentRoute );
  // context.onMounted = ()=>{
  //   console.log(' same-hash-cache onMounted');
  // }
  // context.onUnmount = ()=>{
  //   console.log(' same-hash-cache onUnmount');
  // }
  // context.onReused = ()=>{
  //   console.log(' same-hash-cache onReused');
  // }
  onMounted(context, ()=>{
    console.log(' same-hash-cache onMounted 1');
  }) 
  onUnmount(context, ()=>{
    console.log(' same-hash-cache onUnmount 1');
  }) 
  onReused(context, ()=>{
    console.log(' same-hash-cache onReused 1');
  }) 
  
  
  return (
    <div className="">
      <div className="">
        相同 hash path 场景, 且 cache 时  

        <PartTestA />

      </div>
    </div>
  );
}
export default sameHashCache;


