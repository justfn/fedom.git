const {
  VaryValue,
} = window.$fd;
import "./asyncCpnts.less";

import PartA from "../../../parts/PartA/PartA.js";  // todo: 改成异步加载组件

function asyncCpnts(props, context){
  let pmsCpnt = new Promise((resolve,reject)=>{
    setTimeout(()=>{
      import('../../../parts/PartTestA/PartTestA.js').then((md)=>{
        // let dom = <div className="">
        //   <div className=""> 这个是动态加载的组件 </div>
        //   <md.default />
        // </div>
        // console.log( 'context ', context, dom  );
        // resolve( dom )
        resolve(()=>{
          return <div className="">
            <div className=""> 这个是动态加载的组件 </div>
            <md.default />
          </div>
        })
      })
    },1000 * 3); 
  })
  let v_val = VaryValue(null);
  
  let list = [ 1, <div className="">aaa</div>];
  let fdNd = <i children={'bbb'} />
  let node = document.createElement("input")
  let text = 'ccc'
  let cpnt = <PartA />;
  let valN = null; 
  
  const changeVval = ()=>{
    let num = Math.random() * 10 
    num = Math.floor( num ) % 6;
    let val = '99999';
    if (num===1) {
      val = list;
    }
    else if (num===2) {
      val = fdNd
    }
    else if (num===3) {
      val = node;
    }
    else if (num===0) {
      val = text;
    }
    else if (num===4) {
      val = cpnt;
    }
    else if (num===5) {
      val = valN;
    }
    
    console.log( v_val.$$ );
    console.log( val );
    v_val.$$ = val;
  }
  
  return (
    <div className="">
      <PartA />
      <button 
        children={'动态切换'}
        onClick={changeVval}
      />
      <br />

      { v_val }

      <div className=""> a </div>
      <div className=""> b </div>

      { pmsCpnt }

      <div className=""> x </div>
      <div className=""> y </div>
    </div>
  );
} 
asyncCpnts.scopeName = 'asyncCpnts';
export default asyncCpnts;


