
import FloatCatalog from "../FloatCatalog/index.js";



export default function LayoutA(props, context){
  context.onUnmount = ()=>{
    console.log( 'LayoutA onUnmount ');
  }
  
  let klass = props.className ?? [];
  
  
  return (
    <section data-flg="LayoutA" className={['LayoutA', klass]}>
      { props.children }
      
      <FloatCatalog />
    </section>
  );
}

