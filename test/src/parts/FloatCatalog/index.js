
import "./index.less";

const {
  VaryValue,
  Router,
  Promising,
} = window.$fd;

const isFold = VaryValue(false, (val)=>{
  if (val) { return 'none'; }
  
  return '';
})
const scroll_top = VaryValue(0);
let scroll_top_val = 0;

function FloatCatalog(props, context){
  const router = Router();
  
  const refVal1 = Promising();
  refVal1.then((elem)=>{
    elem.scrollTop = scroll_top.$$;
    // elem.scrollTop = scroll_top_val;
    
    // console.log(' ======================== ');
  })
  context.onReused = ()=>{
    refVal1.then((elem)=>{
      elem.scrollTop = scroll_top.$$;
      // elem.scrollTop = scroll_top_val;
      
      // console.log(' ======================== ');
    })
    
  }
  
  let routes = [ ...router.routes ];
  routes.splice(1, 0, ...[
    {
      path: '/home' 
    },
    {
      path: '/home?key=val' 
    },
  ]);
  routes.splice(6, 0, ...[
    {
      path: '/features/sameHashPath?k=v' 
    },
    {
      path: '/features/sameHashPath?k=v#aa' 
    },
  ]);
  routes.splice(9, 0, ...[
    {
      path: '/features/sameHashCache?k=v' 
    },
    {
      path: '/features/sameHashCache?k=v#aa' 
    },
  ]);
  let links = routes.map((itm,idx)=>{
    return (
      <div>
        <div className="routeItem" 
          onClick={()=>Router.push(itm.path)}>
          { itm.path }
        </div>
      </div>
    )
  })
  
  let foldHandle = ()=>{
    isFold.set((val)=>{
      return !val;
    })
  }
  
  let onScrollHandle = (evt)=>{
    // console.log( evt.currentTarget.scrollTop );
    scroll_top.set((val)=>{
      return evt.currentTarget.scrollTop;
    })
    scroll_top_val = evt.currentTarget.scrollTop;
  }
  
  
  let forwardHandle = ()=>{
    Router.forward();
  }
  let backHandle = ()=>{
    Router.back();
  }
  let goHandle = (num)=>{
    Router.go(num);
  }
  let backUntillToHandle = (num)=>{
    // router.backUntillTo('http://127.0.0.1:9000/#/features');
    router.backUntillTo(
      'http://127.0.0.1:9000/#/featuresa', 
      'http://127.0.0.1:9000/#/',
    );
  }
  
  
  
  return (
    <section className="FloatCatalog" >
      <div 
        className="fold" 
        onClick={foldHandle}
        children={'fold'}
      />
      <div 
        className="linksWp" 
        style={{display: isFold, }}
        onScroll={onScrollHandle}
        ref={refVal1}
      > 
        { links } 
        <div className="" onClick={forwardHandle}>forward</div>
        <div className="" onClick={backHandle}>back</div>
        <div className="" onClick={()=>goHandle(1)}>go 1</div>
        <div className="" onClick={()=>goHandle(2)}>go 2</div>
        <div className="" onClick={()=>goHandle(3)}>go 3</div>
        <div className="" onClick={()=>goHandle(-1)}>go -1</div>
        <div className="" onClick={()=>goHandle(-2)}>go -2</div>
        <div className="" onClick={()=>goHandle(-3)}>go -3</div>
        <div className="" onClick={backUntillToHandle}> backUntillTo </div>
      </div>

      
    </section>
  );
}
export default FloatCatalog;

