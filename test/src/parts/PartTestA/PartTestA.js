const {
  VaryValue,
  onMounted,
  onUnmount,
  onReused,
} = window.$fd;
import "./PartTestA.less";



function PartTestA(props, context){
  // console.log( ' PartTestA props', props );
  // context.onMounted = ()=>{
  //   console.log(' part-test-a onMounted');
  // }
  // context.onUnmount = ()=>{
  //   console.log(' part-test-a onUnmount');
  // }
  // context.onReused = ()=>{
  //   console.log(' part-test-a onReused');
  // }
  onMounted(context, ()=>{
    console.log(' part-test-a onMounted 1');
  }) 
  onUnmount(context, ()=>{
    console.log(' part-test-a onUnmount 1');
  }) 
  onReused(context, ()=>{
    console.log(' part-test-a onReused 1');
  }) 

  
  
  return (
    <div className="PartTestA">
      组件: PartTestA 
      <div className="">
        { props.children }
        <br />
        { props.children1 }
      </div>
    </div>
  );
} 
// PartTestA.scopeName = 'PartTestA';
export default PartTestA;


