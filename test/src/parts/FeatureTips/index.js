const {
  VaryValue,
} = window.$fd;
import "./index.less";


function FeatureTips(param, ctx, cpnts){
  let isUnfold = VaryValue(true);
  let switchContent = ()=>{
    isUnfold.$$ = !isUnfold.$$;
  }
  
  
  return (
    <div className="FeatureTips">
      <div className="FT_tl" >
        <span className="" onClick={switchContent}>使用的特性</span>
        <span className="" data-rotate={isUnfold}>></span>
      </div>
      <div className="FT_ct" fd_show={isUnfold}>
        {
          param.list.map((itm,idx)=>{
            return (
              <div className="FT_itm">
                <div className="FT_tl01"> { itm.title } </div>
                <div className="FT_dt">
                  {
                    itm.list.map((itm1,idx)=>{
                      return (
                        <div className="FT_dt_itm">{ itm1 }</div>
                      )
                    })
                  }
                </div>
              </div>
            )
          })
        }
      </div>

    </div>
  );
} 
// FeatureTips.scopeName = 'FeatureTips';
export default FeatureTips;


