const {
  DecorateSync,
  DecorateAsync,
  DecorateSyncMore,
  DecorateAsyncMore,
} = window.$fd;

import FloatCatalog from "../parts/FloatCatalog/index.js";
import PartA from "../parts/PartA/PartA.js";



const cpntsMap = {
  FloatCatalog: FloatCatalog,
  PartA: PartA,
}
export const decorate_sync = DecorateSync(cpntsMap)
export const decorate_async = DecorateAsync(cpntsMap)
export const decorate_sync_more = DecorateSyncMore((targetCpntFunc)=>{
  return function PageCpnt(props, context){
    return (
      <div className="decorate_page">
        <FloatCatalog />

        { targetCpntFunc(
          props, 
          context, 
          cpntsMap
        ) }
      </div>
    )
  };
})
export const decorate_async_more = DecorateAsyncMore((targetCpntFunc)=>{
  return function PageCpnt(props, context){
    return (
      <div className="decorate_page">
        <FloatCatalog />

        { targetCpntFunc(
          props, 
          context, 
          cpntsMap
        ) }
      </div>
    )
  };
})


