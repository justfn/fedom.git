const {
  Router,
} = window.$fd;
import {
  decorate_sync,
  decorate_sync_more,
  decorate_async,
  decorate_async_more,
} from "./routerConfig.js";
import homePage from "../pages/home/index.js";
import pageSync from "../pages/fealture/pageSync/pageSync.js";
import pageSyncMore from "../pages/fealture/pageSyncMore/pageSyncMore.js";


const routes_ = [
  // 首页
  {
    path: '/',
    // 注意: 统一书写方式,非异步加载也需使用函数返回的形式 
    // component: (prePath, newPath)=>homePage, 
    component: decorate_sync_more(homePage), 
    extra: {
      title: '首页',
    },
    alias: '/home',
    // isCache: true, // bol|fn,是否缓存 
    children: [
      {
        path: 'catalog',
        component: decorate_async_more(()=>{
          return import('../pages/home/catalog/index.js');
        }), 
        extra: {
          title: '目录',
        },
      },
    ],
  },
  // 特性展示 
  {
    path: '/features',
    // alias: '/feature/home',
    component: decorate_async_more(()=>{
      return import('../pages/fealture/fealtureHome.js')
    }),
    children: [
      // 相同 hashPath 
      {
        path: '/sameHashPath',
        component: decorate_async_more(()=>{
          return import('../pages/fealture/sameHashPath/sameHashPath.js')
        }),
        isCache: true, 
      },
      // 相同 hashPath 且 cache 时 
      {
        path: '/sameHashCache',
        component: decorate_async_more(()=>{
          return import('../pages/fealture/sameHashCache/sameHashCache.js')
        }),
        isCache: true, 
      },
      {
        path: 'jsx',
        component: decorate_async_more(()=>{
          return import('../pages/fealture/jsx/jsx.js')
        }),
      },
      // 无法进入的页面 
      {
        path: '/unpass',
        component: decorate_async_more(()=>{
          return import('../pages/fealture/jsx/jsx.js')
        }),
      },
      // 页面逻辑外提 
      {
        path: 'upLogic',
        isCache: true, // bol|fn,是否缓存 
        component: ()=>import('../pages/fealture/upLogic/upLogic.js'),
      },
      // 异步组件 
      {
        path: 'asyncCpnts',
        // isCache: true, // bol|fn,是否缓存 
        component: ()=>import('../pages/fealture/asyncCpnts/asyncCpnts.js'),
      },
      // 装饰 同步组件  
      {
        path: 'd_s',
        component: decorate_sync(pageSync),
      },
      // 默认装饰 同步组件  
      {
        path: 'd_s_m',
        component: decorate_sync_more(pageSyncMore),
      },
      // 装饰 异步组件  
      {
        path: 'd_a',
        component: decorate_async(()=>import('../pages/fealture/pageAsync/pageAsync.js')),
      },
      // 默认装饰 异步组件  
      {
        path: 'd_a_m',
        component: decorate_async_more(()=>import('../pages/fealture/pageAsyncMore/pageAsyncMore.js')),
      },
    ],
  },
  // 应用：特性综合使用 
  {
    path: '/apps',
    component: decorate_async_more(()=>{
      return import('../pages/apps/appHome/appHome.js')
    }),
    isCache: true, // bol|fn,是否缓存 
    children: [
      {
        path: 'TicTacToe',
        component: ()=>import('../pages/apps/TicTacToe/TicTacToe.js'),
      },
      {
        path: 'TicTacToeV2',
        component: ()=>import('../pages/apps/TicTacToeV2/TicTacToe.js'),
      },
      {
        path: 'TodoList',
        component: decorate_async_more(()=>{
          return import('../pages/apps/TodoList/todoList.js')
        }),
      },
    ],
  },
  // 其他测试 
  {
    path: '/test',
    alias: '/test/home',
    // redirect: '/home', 
    component: ()=>import('../pages/test/home/home.js'),
    isCache: true, // bol|fn,是否缓存 
    children: [
      // {
      //   path: 'home',
      //   // component: ()=>import('../pages/test/home/home.js'),
      // },
      {
        path: 'cpntScpoe',
        component: decorate_async_more(()=>{
          return import('../pages/test/cpntScpoe/cpntScpoe.js');
        }),
      },
      {
        path: 'chooseList',
        component: decorate_async_more(()=>{
          return import('../pages/test/chooseList/chooseList.js');
        }),
      },
      {
        path: 'style',
        component: decorate_async_more(()=>{
          return import('../pages/test/style/style.js');
        }),
      },
      {
        path: 'other',
        component: decorate_async_more(()=>{
          return import('../pages/test/other/other.js');
        }),
      },
    ],
  },
]



const options = {
  root: document.querySelector("#app"),
  routes: routes_,
  // beforeEach(oldRoute, newRoute){
  //   if (newRoute.hashPath==='/features/unpass') { 
  //     return false; 
  //   }
  // 
  //   return true;
  // },
  afterEach(oldRoute, currentRoute){
    let routeOption = currentRoute.option || {};
    let routerExtra = routeOption.extra || {};
    let title = routerExtra.title || 'fedom test';
    document.title = title; 
  },
}
const router = Router(options);

export default router; 


