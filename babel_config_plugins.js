/* webpack配置项 
使用: 
  module: {
    rules: [
      // 处理 js 
      {
        test: /\.js$/,
        // exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env',],
            plugins: [
              // 此处引入配置 
              ...require("fedom/babel_config_plugins.js"), 
            ],
          },
        }, 
      },
    ],
  },

*/


module.exports = [
  // 支持 jsx 编译 
  [
    '@babel/plugin-transform-react-jsx', 
    { 
      pragma: 'window.$fd.compiler', // 定义的全局编译方法 
    },
  ],
]

