const {
  VaryValue, 
  Router,
} = window.$fd;
import "./FloatCatalog.less";

let is_show_content = VaryValue(false); // 

function FloatCatalog(props, context){
  let router = Router();
  let routes = router.routes;
  // console.log( routes );
  
  let changeRolate = ()=>{
    is_show_content.$$ = !is_show_content.$$;
  }
  
  return (
    <div className="FloatCatalog">
      <div className="title" onClick={changeRolate}>
        路由列表
        <span data-rotate={is_show_content}>^</span>
      </div>
      <div className="content" fd_show={is_show_content}>
        {
          routes.map((itm,idx)=>{
            return (
              <div className="itm" 
                onClick={()=>{Router.push(itm.path)}}>
                { new Array(itm.level+1).fill('').map((itm,idx)=>{
                  return '>'
                }) }
                { itm.extra.title }
              </div>
            )
          })
        }
      </div>
    </div>
  );
} 
FloatCatalog.scopeName = 'FloatCatalog';
export default FloatCatalog;


