const {
  Router,
} = window.$fd;
import { decorateSyncPageFunc } from "../logics/decorate_page.js";
import { decoratePageFunc } from "../logics/decorate_page.js";
import { decoratePageFuncWith } from "../logics/decorate_page.js";
import homePage from "../pages/home/home.js";


const routes_ = [
  {
    path: '/',
    // 注意非异步加载也需使用函数返回的形式 
    // component: (pre, current)=>homePage, 
    // component: ()=>import('../pages/home/home.js'),
    // component: decorateSyncPageFunc(homePage),
    component: decoratePageFuncWith(()=>import('../pages/home/home.js')),
    alias: '/home',
    // isCache: true, // bol|fn,是否缓存 
    extra: {
      title: 'home',
    },
    children: [
      // {
      // 
      // },
    ],
  },
  {
    path: '/hello',
    component: decoratePageFuncWith(()=>import('../pages/hello/hello.js')),
    extra: {
      title: 'hello',
    },
    children: [
      // {
      // 
      // },
    ],
  },
]



const options = {
  routes: routes_,
  root: document.querySelector("#app"),
  // beforeEach(pre, nxt){
  //   if (nxt.path==='/features/unpass') { return false; }
  // 
  //   return true;
  // },
  afterEach(pre, current){
    console.log( pre, current);
    if ( current.option.extra ) {
      document.title = current.option.extra.title;
    }
  },
}
const router = Router(options);

export default router; 


