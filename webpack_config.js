
const pathLib = require("path");
// const Webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: "production",
  // 单文件入口 
  entry: pathLib.resolve(__dirname, './src/main.js'),
  output: {
    // 定义输出文件夹dist路径
    path: pathLib.resolve(__dirname, './dist'),
    // 定义输出文件名
    filename: '[name].js',   
    // library: 'fedom',
  },
  module: {
    rules: [
      // 处理 js 
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env',],
            plugins: [
              // 支持修饰器语法 
              [
                "@babel/plugin-proposal-decorators", 
                { "legacy": true }
              ],
              // 支持 class 语法  
              '@babel/plugin-proposal-class-properties',
              // ...require("fedom/babel_config_plugins.js"), 
              ...require("./babel_config_plugins.js"), 
            ],
          },
        }, 
      },
    ],
  },
  plugins: [
    // 每次打包前删除dist文件夹中的文件
    new CleanWebpackPlugin({  
      //dist文件夹下的favicon.ico文件和lib文件夹下的东西都忽略不进行删除
      // cleanOnceBeforeBuildPatterns: ['**/*', '!favicon.ico', '!lib/**'],
    }),
  ],
}


