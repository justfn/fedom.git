import FloatCatalog from "../parts/FloatCatalog/FloatCatalog.js";

const cpts = {
  FloatCatalog,
};

/* ** 函数页面组件装饰器-同步加载*/
export function decorateSyncPageFunc(cptFunc){
  return function(pre, current){
    function Page(props, context){
      return cptFunc(
        props, 
        context, 
        pre, 
        current,
        cpts
      );
    };
    Page.scopeName = cptFunc.scopeName || cptFunc.name;
    return Page;
  };
} 
/* ** 函数页面组件装饰器-异步加载*/
export function decoratePageFunc(pmsFunc){
  return function(pre, current){
    return pmsFunc().then((md)=>{
      function Page(props, context){
        return md.default(
          props, 
          context, 
          pre, 
          current,
          cpts
        );
      }
      Page.scopeName = md.default.scopeName || md.default.name;
      return {
        default: Page,
      };
    })
  };
} 
/* ** 函数页面组件装饰器-异步加载-带默认组件渲染 */
export function decoratePageFuncWith(pmsFunc){
  return function(pre, current){
    return pmsFunc().then((md)=>{
      function Page(props, context){
        return (
          <div className="decorate_page">
            <FloatCatalog />
            
            { md.default(
              props, 
              context, 
              pre, 
              current,
              cpts
            ) }
          </div>
        )
      };
      Page.scopeName = md.default.scopeName || md.default.name;
      return {
        default: Page,
      };
    })
  };
} 

