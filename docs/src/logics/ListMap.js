

function getListMap(list, mainKey){
  return {
    list, 
    _map: null,
    get map(){
      if (this._map) {
        return this._map;
      }
      
      let mObj = {};
      list.forEach((itm,idx)=>{
        mObj[ itm[mainKey] ] = {
          ...itm,
        };
      })
      this._map = mObj;
      return this._map;
    },
  };
} 
export default getListMap;

