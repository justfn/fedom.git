const {
  VaryValue, 
} = window.$fd;
import "./ApiImportShow.less";
import ListMap from "../../logics/ListMap.js";
import CodeBrackets from "../CodeBrackets/CodeBrackets.js";


function ApiImportShow(props, context){
  const {
    list,
  } = props;
  let khLft = '{';
  let khRit = '}';
  
  let apiListMap = ListMap([
    {
      field: 'render',
      label: 'render',
      comment: '渲染方法', 
      isPartBtm: true,
    },
    {
      field: 'VaryValue',
      label: 'VaryValue',
      comment: '包装任意值', 
      isPartBtm: false,
    },
    {
      field: 'VaryList',
      label: 'VaryList',
      comment: '包装list类型值', 
      isPartBtm: false,
    },
    {
      field: 'VaryMap',
      label: 'VaryMap',
      comment: '包装map类型值', 
      isPartBtm: true,
    },
    {
      field: 'onMounted',
      label: 'onMounted',
      comment: '生命周期-组件渲染后 ', 
      isPartBtm: false,
    },
    {
      field: 'onUnmount',
      label: 'onUnmount',
      comment: '生命周期-组件卸载时 ', 
      isPartBtm: false,
    },
    {
      field: 'onReused',
      label: 'onReused',
      comment: '生命周期-使用缓存时 ', 
      isPartBtm: false,
    },
    {
      field: 'Component',
      label: 'Component',
      comment: 'class类组件 ', 
      isPartBtm: true,
    },
    {
      field: 'Router',
      label: 'Router',
      comment: '路由功能 ', 
      isPartBtm: true,
    },
    {
      field: 'Promising',
      label: 'Promising',
      comment: '待状态变更的Promise ', 
      isPartBtm: false,
    },
    {
      field: 'utils',
      label: 'utils',
      comment: '一些工具方法 ', 
      isPartBtm: false,
    },
  ], 'field');
  let listShow = [];
  if ( !list ) {
    listShow = apiListMap.list;
  }
  else {
    let map = apiListMap.map;
    listShow = list.map((itm,idx)=>{
      return map[itm];
    })
  }
  
  return (
    <section className="">
      
      const {' '} <CodeBrackets>
        <div className="">
          {
            listShow.map((itm,idx)=>{
              return [
                itm.label + ',',
                `// ${itm.comment}`,
                <br />,
                itm.isPartBtm&&idx!=listShow.length-1 ? <br/> : null,
              ]
            })
          }
        </div>
      </CodeBrackets>
      {' '} = window.$fd;
    </section>
  );
} 
// ApiImportShow.scopeName = 'ApiImportShow';
export default ApiImportShow;
