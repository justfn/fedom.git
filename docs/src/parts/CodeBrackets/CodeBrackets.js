const {
  VaryValue, 
} = window.$fd;


function CodeBrackets(props, context){
  let khLft = '{'
  let khRit = '}'
  
  
  return (
    <span className="">
      { khLft }
      <p> { props.children } </p>
      { khRit }
    </span>
  );
} 
CodeBrackets.scopeName = 'CodeBrackets';
export default CodeBrackets;


