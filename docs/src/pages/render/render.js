const {
  VaryValue, 
} = window.$fd;
import ApiImportShow from "../../parts/ApiImportShow/ApiImportShow.js";


function render_page(props, context){
  let txt1 = `
    <div>
      这是一段被渲染的文本
    </div>
  `
  
  
  return (
    <div className="render_page">
      <h1> 视图渲染 render </h1>
      
      <div className="">
        fedom 使用 jsx 模板来渲染, 站在巨人的肩膀上, render 增加了些功能  
        <div className="indent_area">
          <div className="">
            <div className="code_area">
              <ApiImportShow  
                list={['render']}
              />
              <br />
              // 获取容器节点, 后续都在该容器内渲染内容 
              <br />
              const rootNode = document.getElementById("app"); 
            </div>
            
            <p>
              渲染 jsx标签 (即fdNode节点) 
            </p>
            <div className="code_area">
              let result = render({txt1}, rootNode) 
            </div>
            
            <p>
              渲染 文本 
            </p>
            <div className="code_area">
              let result = render({"'这是一段被渲染的文本'"}, rootNode) 
            </div>
            
            <p>
              渲染 DOM节点 
            </p>
            <div className="code_area">
              let divDom = document.createElement("div");
              <br />
              divDom.textContent = '这是一段被渲染的文本'
              <br />
              let result = render(divDom, rootNode);
            </div>
            
            <p>
              渲染 '动态内容' 
            </p>
            <div className="code_area">
              let v_dom = VaryValue('aaa');
              <br />
              let result = render( v_dom, rootNode );
              <br />
              setTimeout(()=>v_dom.$$='bbb',3000) // 3秒后改变 
              <br />
              <br />
              
              let v_dom = VaryValue( { txt1 } );
              <br />
              let result = render( v_dom, rootNode );
              <br />
              setTimeout(()=>v_dom.$$='bbb',3000) // 3秒后改变 
              <br />
              <br />
               
              let v_dom = VaryValue(document.createElement("input"));
              <br />
              let result = render( v_dom, rootNode );
              <br />
              setTimeout(()=>v_dom.$$='bbb',3000) // 3秒后改变 
              <br />
              <br />

              let v_dom = VaryValue( [
                'aaa',
                { txt1 },
                VaryValue('ccc')
              ] );
              <br />
              let result = render( v_dom, rootNode );
              <br />
              setTimeout(()=>v_dom.$$='bbb',3000) // 3秒后改变 
            </div>
            
            <p>
              渲染 数组列表 
            </p>
            <div className="code_area">
              let result = render( [
                { txt1 },
                'aaaa'
              ], rootNode );
            </div>
          </div>
        </div>
      </div>
      
      
    </div>
  );
} 
render_page.scopeName = 'render_page';
export default render_page;


