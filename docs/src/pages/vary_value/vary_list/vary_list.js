const {
  VaryValue, 
} = window.$fd;
import "./vary_list.less";

function vary_list(props, context){
  
  return (
    <div className="vary_list">
      <h1>动态列表</h1>
      <div>
        继承自 VaryValue, 为优化list数据结构的渲染更新; 
        <br />
        VaryValue 变动时, 将会更新范围内的所有相关节点, 
        <br />
        而当一list只需更新其中一项时, 使用 VaryValue 则需要更新整个list,
        <br />
        所以此时 VaryList, 就是主要用于解决该情景的方案;
      </div>
    
      <br />
    </div>
  );
} 
vary_list.scopeName = 'vary_list';
export default vary_list;


