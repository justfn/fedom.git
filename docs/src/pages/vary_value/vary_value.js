const {
  VaryValue, 
} = window.$fd;
import "./vary_value.less";
import ApiImportShow from "../../parts/ApiImportShow/ApiImportShow.js";
import CodeBrackets from "../../parts/CodeBrackets/CodeBrackets.js";

function vary_value(props, context){
  
  let txt1 = `
    <div fd_if={v_val}> ⭐️ </div>
  `
  let txt2 = `
    <div fd_show={v_val}> ⭐️ </div>
  `
  
  return (
    <div className="vary_value">
      <h1> 动态值: 用于初始渲染后的视图更新 </h1>
      
      <div className="indent_area">
        <div className="code_area">
          <ApiImportShow 
            list={['render','VaryValue']}
          />
          <br />
          
          例子1: 计时器 
          <br />
          let v_val = VaryValue(0, (val)=><CodeBrackets>
            <div className="">
              return '已经过时间: '+ val +' s'
            </div>
          </CodeBrackets>);
          <br />
          render(v_val, rootNode);
          <br />
          setInterval(()=><CodeBrackets>
            <div className="">
              v_val.set(preVal=><CodeBrackets>
                <div className="">
                  return preVal+1;
                </div>
              </CodeBrackets>)
            </div>
          </CodeBrackets>,1000)
        </div>
        
        <h3 children={'为了便捷控制节点显示, 提供了两个额外动态属性'} />
        <div className=""> fd_if 动态控制节点是否存在</div>
        <div className=""> fd_show 动态控制节点是否显示</div>
        <div className="code_area">
          例子2: 闪烁的星星
          <br />
          let v_val = VaryValue(true);
          <br />
          render( [
            <div className="">
              { txt1 },
              <br />
              { txt2 },
            </div>
          ], rootNode);
          <br />
          setInterval(()=>v_val.set(pV=>!pV),1000)
        </div>
        
        
      </div>
      
    </div>
  );
} 
// vary_value.scopeName = 'vary_value';
export default vary_value;


