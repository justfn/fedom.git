const {
  VaryValue, 
} = window.$fd;
import "./home.less";
import ApiImportShow from "../../parts/ApiImportShow/ApiImportShow.js";
import CodeBrackets from "../../parts/CodeBrackets/CodeBrackets.js";

function home_page(props, context, pre, nxt, Cpts){
  // console.log( props, context );
  
  return (
    <div className="home_page">
      <h1>文档首页</h1>

      <h3>fedom 介绍 </h3>
      <div className="indent_area">
        fedom: 前端DOM视图渲染解决方案 
        <div className="">
          借鉴 React 和 Vue3 等框架的优点及个人的认知、思考 
        </div>
        <div className="">
          力求一个 足够简单 & 功能完善 & 使用便捷 & 灵活高效 的前端视图渲染框架 
          <div className="">
            足够简单: 无虚拟dom, 无过多依赖, 初始化一次性渲染 
          </div>
          <div className="">
            功能完善: 满足dom更新, 路由等前端应用的操作功能 
          </div>
          <div className="">
            使用便捷: model 到 view 数据驱动, 仅需 VaryValue 和 jsx 两个核心概念 
          </div>
          <div className="">
            灵活自由: 
            <div className="">
              无过度限制 自由的JS使用方式 
            </div>
            <div className="">
              灵活的拆解和组装 组件生命周期类hook使用方式 
            </div>
            <div className="">
              VaryValue 实现无任何限制的dom更新 
            </div>
            <div className="">
              css命名空间 (编译时已标识组件节点,配合webpack loader 实现) 
            </div>
          </div>
        </div>
      </div>

      <h3> 安装配置 </h3>
      <div className="indent_area">
        <div className="">
          安装fedom框架依赖
          <div className="code_area"> npm i fedom -S </div>
        </div>

        <div>
          配置webpack 的 module.rules 选项, 解析js文件
          <div className="code_area">
            <CodeBrackets>
              <div className="">
                test: /\.js$/,
                <br />
                exclude: /node_modules/,
                <br />
                use: <CodeBrackets>
                  <div>
                    loader: 'babel-loader',
                    <br />
                    options: <CodeBrackets>
                      <div>
                        presets: ['@babel/preset-env',],
                        <br />
                        plugins: [
                        <div className="">
                          // 其他配置 ... 
                        </div>
                        <div>
                          ...require("fedom/babel_config_plugins.js"), 
                        </div>
                        ],
                      </div>
                    </CodeBrackets>
                  </div>
                </CodeBrackets>
              </div>
            </CodeBrackets>
          </div>
        </div>
      </div>

      <h3> 使用 </h3>
      <div className="indent_area">
        <div className="code_area">
          <div className="">
            import "fedom"; // 引入fedom框架库  
            <br />
            // 对外接口全部通过 window.$fd 输出 
            <br />
            <br />

            <ApiImportShow />
          </div>
        </div>
      </div>
    </div>
  );
} 
home_page.scopeName = 'home_page';
export default home_page;


