const {
  Router,
} = window.$fd;
import { decorateSyncPageFunc } from "../logics/decorate_page.js";
import { decoratePageFunc } from "../logics/decorate_page.js";
import { decoratePageFuncWith } from "../logics/decorate_page.js";
import homePage from "../pages/home/home.js";


const routes_ = [
  {
    path: '/',
    // 注意非异步加载也需使用函数返回的形式 
    // component: (pre, current)=>homePage, 
    // component: ()=>import('../pages/home/home.js'),
    // component: decorateSyncPageFunc(homePage),
    component: decoratePageFuncWith(()=>import('../pages/home/home.js')),
    alias: '/home',
    // isCache: true, // bol|fn,是否缓存 
    extra: {
      title: 'home',
    },
    children: [
      // {
      // 
      // },
    ],
  },
  {
    path: '/render',
    component: decoratePageFuncWith(()=>import('../pages/render/render.js')),
    extra: {
      title: 'render',
    },
    children: [
      // {
      // 
      // },
    ],
  },
  {
    path: '/vary_value',
    component: decoratePageFuncWith(()=>import('../pages/vary_value/vary_value.js')),
    extra: {
      title: 'VaryValue',
    },
    children: [
      {
        path: '/vary_list',
        component: decoratePageFuncWith(()=>import('../pages/vary_value/vary_list/vary_list.js')),
        extra: {
          title: 'VaryList',
        },
      },
      {
        path: '/vary_map',
        component: decoratePageFuncWith(()=>import('../pages/vary_value/vary_map/vary_map.js')),
        extra: {
          title: 'VaryMap',
        },
      },
    ],
  },
]



const options = {
  routes: routes_,
  root: document.querySelector("#app"),
  // beforeEach(pre, nxt){
  //   if (nxt.path==='/features/unpass') { return false; }
  // 
  //   return true;
  // },
  afterEach(pre, current){
    console.log( pre, current);
    if ( current.option.extra ) {
      document.title = current.option.extra.title;
    }
  },
}
const router = Router(options);

export default router; 


